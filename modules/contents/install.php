<?php
class contents_install extends install_modules
{
	function install($id_module, $reinstall = false)
	{
		global $kernel;

        $query = 'CREATE TABLE IF NOT EXISTS `'.$kernel->pub_prefix_get().'_content_blocks` (
             `id` int(10) unsigned NOT NULL auto_increment,
             `module_id` varchar(255) NOT NULL,
             `name` text NOT NULL,
             `description` text DEFAULT NULL,
             PRIMARY KEY  (`id`),
             KEY `module_id` (`module_id`)
         ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        $kernel->runSQL($query);
		
        $query = "CREATE TABLE `".$kernel->pub_prefix_get()."_content_fields` (
			`id` int(10) unsigned NOT NULL AUTO_INCREMENT,
			`module_id` varchar(255) NOT NULL,
			`field_type` enum('select','string','pagesite','textarea','checkbox','fileselect','imageselect','editor') NOT NULL,
			`field_title` varchar(255) NOT NULL,
			`field_name` varchar(255) NOT NULL,
			`field_order` int(5) unsigned NOT NULL,
			`field_params` text,
			PRIMARY KEY (`id`),
			KEY `moduleid_order` (`module_id`,`field_order`)
            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1";
        $kernel->runSQL($query);
	}
	
	function uninstall($id_module)
	{
		global $kernel;
        $query = 'DROP TABLE `'.$kernel->pub_prefix_get().'_content_blocks`';
		$kernel->runSQL($query);
        $query = 'DROP TABLE `'.$kernel->pub_prefix_get().'_content_fields`';
		$kernel->runSQL($query);
	}

	
	function install_children($id_module, $reinstall = false)
	{
		global $kernel;
	    $query = 'CREATE TABLE IF NOT EXISTS `'.$kernel->pub_prefix_get().'_'.$id_module.'` (
             `id` int(10) unsigned NOT NULL auto_increment,
             `post_date` DATE default NULL,
             `block_id` int(10) unsigned DEFAULT "0",
             `order` int(10) unsigned,
             PRIMARY KEY  (`id`),
             KEY `order` (`order`),
             KEY `block_id` (`block_id`)
         ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1';
        $kernel->runSQL($query);
	}

	function uninstall_children($id_module)
	{
		global $kernel;
        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_content_blocks` WHERE `module_id`="'.$id_module.'"';
        $query = 'DELETE FROM `'.$kernel->pub_prefix_get().'_content_fields` WHERE `module_id`="'.$id_module.'"';
		$kernel->runSQL($query); 
		
		$query = 'DROP TABLE `'.$kernel->pub_prefix_get().'_'.$id_module.'`';
        $kernel->runSQL($query);
	}
	
}


$install = new contents_install();

$install->set_name('[#contents_modul_base_name#]');
$install->set_id_modul('contents');
$install->set_admin_interface(2);

$install->add_public_metod('pub_show_content', '[#pub_show_content#]');

$property = new properties_file();
$property->set_id('template');
$property->set_caption('[#contents_template#]');
$property->set_patch('modules/contents/templates_user');
$property->set_mask('htm,html');
$property->set_default('modules/contents/templates_user/template_user.html');
$install->add_public_metod_parametrs('pub_show_content',$property);

$property = new properties_select();
$property->set_id("sorting");
$property->set_caption("[#content_fields_field_order#]");
$property->set_data(array ("asc"=>"[#contents_property_sort_ask#]",
                           "desc"=>"[#contents_property_sort_desk#]",
                           "orderAsc"=>"[#contents_property_sort_order_num_ask#]",
                           "orderDesc"=>"[#contents_property_sort_order_num_desk#]",
                           "random"=>"[#contents_property_sort_random#]"));
$install->add_public_metod_parametrs('pub_show_content',$property);

$property = new properties_select();
$property->set_caption("[#contents_select_blocks#]");
$property->set_default('null');
$property->set_id('block_id');
$property->set_data_user_func("modules/contents/contents.class.php", array("contents", 'get_blocks_select'));
$install->add_public_metod_parametrs('pub_show_content', $property);

$property = new properties_string();
$property->set_id('num_items');
$property->set_caption("[#contents_num_items#]");
$property->set_default(0);
$property->set_description('[#contents_num_items_description#]');
$install->add_public_metod_parametrs('pub_show_content', $property);

$install->module_copy[0]['name'] = 'contents_modul_base_name1';
$install->module_copy[0]['action'][0]['caption']    = '[#pub_show_content#]';
$install->module_copy[0]['action'][0]['id_metod']   = 'pub_show_content';

?>