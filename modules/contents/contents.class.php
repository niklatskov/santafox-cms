<?php
/**
 * Модуль "Блоки контента"
 *
 * @name contents
 * @description Организация и вывод произвольных блоков (списков) контента.
 * Позволяет строить неограниченное кол-во списков самой разной
 * конфигурации, облегчая тем самым работу контент-менеджера. Также, будет
 * полезен для построения всевозможных листингов на основе фреймворка bootstrap.
 *
 * @author Vyshnyvetskyy Alexsander (alex.vyshnyvetskyy@gmail.com)
 * @copyright W.D.M.Group, Ukraine (с) 2009-2016 (http://wdmg.com.ua/)
 * @version 1.1
 *
 */

require_once dirname(dirname(dirname(__FILE__)))."/include/basemodule.class.php";

class contents  extends basemodule
{

	/**
	 * Публичный метод для вывода блока контента
	 *
	 * @param string $template - шаблон вывода
	 * @param string $sorting - порядок сортировки
	 * @param string $block_id - выбранный блок-контента
	 * @return string
	 */
	public function pub_show_content($template, $sorting='asc', $block_id=null, $num_items=0)
	{
		global $kernel;
		$curr_block = false;
		$moduleid = $kernel->pub_module_id_get();
		$this->set_templates($kernel->pub_template_parse($template));
		$cond = "true";

		if (!empty($block_id)) {
			$cond .= " AND `block_id`='".$block_id."'";
			$curr_block = $this->get_block($block_id);
		} else {
			return $this->get_template_block('no_data');
		}

		switch ($sorting) {
			case 'asc':
				$cond.=" ORDER BY `id` ASC";
				break;
			case 'desc':
				$cond.=" ORDER BY `id` DESC";
				break;
			case 'orderAsc':
				$cond.=" ORDER BY `order` ASC";
				break;
			case 'orderDesc':
				$cond.=" ORDER BY `order` DESC";
				break;
			case 'random':
				$cond.=" ORDER BY RAND()";
				break;
			default:
				$cond.=" ORDER BY `id` ASC";
				break;
		}

		$items = $kernel->db_get_list_simple("_".$moduleid, $cond, "*");
		if (count($items)==0)
			return $this->get_template_block('no_data');

        $i = 1;
		$lines = '';
		$fields = $this->get_content_fields($moduleid);
		$templates['row_odd'] = $this->get_template_block('row_odd');
		$templates['row_even'] = $this->get_template_block('row_even');
		$templates['row'] = $this->get_template_block('row');
		
		foreach($items as $data) {
				
			$row_odd = $templates['row_odd'];
			$row_even = $templates['row_even'];
            if (!empty($row_odd) && !empty($row_even) && !(boolean)($i % 2))
                $line = $row_odd;
            else if (!empty($row_odd) && !empty($row_even) && (boolean)($i % 2))
                $line = $row_even;
            else
                $line = $templates['row'];

			$line = $this->priv_content_fields($fields, $data, $line);
			$line = str_replace('%id%', $data['id'], $line);
			$line = str_replace('%order%', $data['order'], $line);
			$line = str_replace('%date%', $data['post_date'], $line);
			$lines .= $line;
			
			if(intval($num_items) > 0 && $i >= intval($num_items))
				break;
			else
            	$i++;
			
		}
		$content = $this->get_template_block('content');
		$content = str_replace('%rows%', $lines, $content);

		if ($curr_block['name']) {
			$block_name = $this->get_template_block('block_name');
			$block_name = str_replace('%block_name%', $curr_block['name'], $block_name);
			$content = str_replace('%block_name%', $block_name, $content);
		}

		if ($curr_block['description']) {
			$block_desc = $this->get_template_block('block_description');
			$block_desc = str_replace('%block_description%', $curr_block['description'], $block_desc);
			$content = str_replace('%block_description%', $block_desc, $content);
		}

		$content = $this->clear_left_labels($content);
		return $content;
	}

	/**
	 * Функция для построения меню административного интерфейса
	 *
	 * @param pub_interface $menu Обьект класса для управления построением меню
	 * @return boolean true
	 */
	function interface_get_menu($menu)
	{
		$menu->set_menu_block('[#contents_menu_block_label#]');
		$menu->set_menu("[#contents_menu_blocks#]","show_blocks");
		$menu->set_menu("[#contents_menu_fields#]","content_fields");
		$menu->set_menu_default('show_blocks');
		return true;
	}

	/**
	 * Функция для построения административного интерфейса модуля
	 *
	 * @return string
	 */
	function start_admin()
	{
		global $kernel;
		$content = '';
		$select_menu = $kernel->pub_section_leftmenu_get();
		$moduleid = $kernel->pub_module_id_get();
		$this->set_templates($kernel->pub_template_parse("modules/contents/templates_admin/template_admin.html"));
		switch ($select_menu)
		{
			case "block_delete":
				$blockid=intval($kernel->pub_httpget_get('id'));
				$kernel->runSQL("UPDATE `".$kernel->pub_prefix_get()."_".$moduleid."` SET `block_id`=0 WHERE `block_id`=".$blockid." AND module_id='".$moduleid."'");
				$kernel->runSQL("DELETE FROM `".$kernel->pub_prefix_get()."_content_blocks` WHERE id=".$blockid);
				$kernel->pub_redirect_refresh('show_blocks');
				break;
			case "block_edit":
				$block = $this->get_block(intval($kernel->pub_httpget_get('blockid')));
				if (!$block)
					$block=array('id'=>0, 'name'=>'');
				$content = $this->get_template_block('block_form');

				$editor = new edit_content();
				$editor->set_edit_name('description');
				$editor->set_simple_theme(true);
				if (isset($block['description']))
					$editor->set_content($block['description']);
				else
					$editor->set_content('');
				$content =str_replace('%description%',$editor->create(),$content);


				$content = str_replace('%action%', $kernel->pub_redirect_for_form('block_save'), $content);
				foreach ($block as $k=>$v)
				{
					$content = str_replace('%'.$k.'%', htmlspecialchars($v), $content);
				}
				break;
			case "block_save":
				$id=intval($kernel->pub_httppost_get('id'));
				$name=$kernel->pub_httppost_get('name');
				$descr=$kernel->pub_httppost_get('description');
				if ($id==0)
					$q="INSERT INTO `".$kernel->pub_prefix_get()."_content_blocks` (`name`,`module_id`,`description`) VALUES
                                           ('".$name."','".$moduleid."','".$descr."')";
				else
					$q="UPDATE `".$kernel->pub_prefix_get()."_content_blocks` SET `description`='".$descr."',`name`='".$name."' WHERE id=".$id;
				$kernel->runSQL($q);
				$kernel->pub_redirect_refresh_reload('show_blocks');
				break;
			case "show_blocks":
				$content = $this->get_template_block('blocks_list');
				$blocks = $this->get_blocks($moduleid);
				$rows = '';
				foreach ($blocks as $block)
				{
					$count = $this->get_items_count($moduleid, $block['id']);
					$row = $this->get_template_block('blocks_row');
					$row = str_replace('%id%', $block['id'], $row);
					$row = str_replace('%name%', $block['name'], $row);
					$row = str_replace('%count%', $count, $row);
					$rows.=$row;
				}
				$content = str_replace('%rows%', $rows, $content);
				break;
			case "item_save":
				$id = intval($kernel->pub_httppost_get('id'));
				$block_id = intval($kernel->pub_httppost_get('blockid'));
				$order = intval($kernel->pub_httppost_get('order'));
				$fields = $this->get_content_fields($moduleid);

				if ($id==0) {
					$field_names = '';
					$field_values = '';

					foreach ($fields as $field) {
						$curr_field = $kernel->pub_httppost_get($field['field_name']);
						$field_names .= ', `'.$field['field_name'].'`';
                        if(!empty($curr_field)) {
							if($field['field_type']=='checkbox')
								$field_values .= ", '".serialize($curr_field)."'";
							else
								$field_values .= ', "'.$curr_field.'"';
						} else {
							$field_values .= ', ""';
						}
					}
					$query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_'.$moduleid.'` (`block_id`, `order`, `post_date`'.$field_names.') VALUES('.$block_id.', "'.$order.'","'.date("Y-m-d").'"'.$field_values.');';
				}
				else
				{
					$field_values = '';
					foreach ($fields as $field) {
						$curr_field = $kernel->pub_httppost_get($field['field_name']);
                        if(!empty($curr_field)) {
							if($field['field_type']=='checkbox')
								$field_values .= ", `".$field['field_name']."` = '".serialize(array_map('trim', $curr_field))."'";
							else
								$field_values .= ', `'.$field['field_name'].'` = "'.$curr_field.'"';
						} else {
                            $field_values .= ', `'.$field['field_name'].'` = ""';
                        }
					}
					$query = 'UPDATE `'.$kernel->pub_prefix_get().'_'.$moduleid.'` SET `block_id`='.$block_id.', `order`="'.$order.'"'.$field_values.' WHERE id='.$id;
				}

				$kernel->runSQL($query);
				$kernel->pub_redirect_refresh_reload("set_left_menu&leftmenu=show_contents&blockid=".$block_id);
				break;

			case "item_delete":
				$item=$kernel->db_get_record_simple("_".$moduleid, "`id`=".intval($kernel->pub_httpget_get('item_id')));
				if ($item)
					$this->item_delete($item);
				$kernel->pub_redirect_refresh('show_contents');
				break;

			case 'show_contents':
				$perpage = 10;
				$total = 0;
				$offset = intval($kernel->pub_httpget_get('offset'));
				$moduleid = $kernel->pub_module_id_get();

				$cond = "true";
				$blockid = intval($kernel->pub_httpget_get('blockid'));
				if ($blockid>0)
					$cond.=" AND `block_id`='".$blockid."'";
				
				$sortby = $kernel->pub_httpget_get('sortby');
				
				switch ($sortby)
				{
					case 'order':
					default:
						$cond.=" ORDER BY `order`";
						break;
					case 'date':
						$cond.=" ORDER BY `post_date`";
						break;
					case 'id':
						$cond.=" ORDER BY `id`";
						break;
				}
				
				$items = $kernel->db_get_list_simple("_".$moduleid, $cond, "*", $offset, $perpage);
				$blocks = $this->get_blocks($moduleid);
				$crec = $kernel->db_get_record_simple("_".$moduleid, $cond, "count(*) AS count");

				if ($crec)
					$total = $crec['count'];

				$lines='';
				foreach ($items as $item)
				{
					$line = $this->get_template_block('line');
					$line = str_replace("%id%", $item['id'], $line);
					$line = str_replace("%module_id%", $moduleid, $line);

					$description = '';
					$fields = $this->get_content_fields($moduleid);

					foreach ($fields as $field)
					{
						if(!empty($item[$field['field_name']]))
							$description .= $item[$field['field_name']]." ";
					}

					$description = strip_tags($description);
					$description = substr($description, 0, 480);
					$description = rtrim($description, "!,.-");
					$description = substr($description, 0, strrpos($description, ' '))."…";
					$line = str_replace("%content%",  $description,     $line);

					$line = str_replace("%order%",  		  $item['order'],     $line);
					$line = str_replace("%post_date%",    $item['post_date'],       $line);
					$lines .= $line;
				}
				$content = $this->get_template_block('begin').$lines.$this->get_template_block('end');

				$block_name='';
				$blocks_lines='';
				foreach ($blocks as $block)
				{
					if ($blockid==$block['id']) {
						$block_name = '"'.$block['name'].'"';
					}
				}
				$content = str_replace('%blockid%', $blockid, $content);
				$content = str_replace("%sortby%", $sortby, $content);
				$purl='show_contents&blockid='.$blockid.'&sortby='.$sortby.'&offset=';
				$content = str_replace('%pages%', $this->build_pages_nav($total,$offset,$perpage,$purl,0,'url'), $content);
				$content = str_replace("%action%", $kernel->pub_redirect_for_form("process_selected"), $content);
				$content = str_replace('%block_name%', $block_name, $content);
				break;
			case "process_selected":
				$post = $kernel->pub_httppost_get();
				if(!isset($post['action']))
					$kernel->pub_redirect_refresh('show_contents');

				$items = array();
				if(isset($post['item']))
					$items = array_keys($post['item']);

				if ($post['action'] == 'delete_selected' && isset($post['blockid'])) {
					$cond = "`id` IN (".implode(",", $items).") AND `block_id` = '".$post['blockid']."'";
					$deleteitems = $kernel->db_get_list_simple('_'.$moduleid, $cond);
					foreach ($deleteitems AS $item) {
						$this->item_delete($item);
					}
				} else if ($post['action'] == 'delete_all' && isset($post['blockid'])) {
					$cond = "`block_id` = '".$post['blockid']."'";
					$deleteitems = $kernel->db_get_list_simple('_'.$moduleid, $cond);
                    foreach ($deleteitems AS $item) {
						$this->item_delete($item);
					}
				} else {
                    $orders = $kernel->pub_httppost_get("order");
                    foreach ($orders as $item_id => $order)
                    {
                        if (is_numeric($order)) {
                            $query = "UPDATE `".$kernel->pub_prefix_get()."_".$moduleid."` SET `order`=".$order." WHERE `id`=".$item_id;
                            $kernel->runSQL($query);
                        }
                    }
                }

				$kernel->pub_redirect_refresh_reload("show_contents");
				break;
			case "item_edit":
				$id = intval($kernel->pub_httpget_get('item_id'));
				$item = $this->get_item($id);
				if (!$item)
				{
					$cond = "true";
					$blockid = intval($kernel->pub_httpget_get('blockid'));
					if ($blockid>0)
						$cond.=" AND `block_id`='".$blockid."'";
					
					$maxid = $kernel->db_get_record_simple("_".$moduleid, $cond, "MAX(`order`) as maxid");
					$item = array('id'=>0, 'title_image'=>'', 'order'=>($maxid ? $maxid['maxid']+5 : 1),'description'=>'','image'=>'','block_id'=>0);
					$content = $this->get_template_block('form_add');
				} else {
					$content = $this->get_template_block('form_edit');
					$blockid = $item['block_id'];
				}

				$fields_list = '';
				$fields = $this->get_content_fields($moduleid);
				foreach ($fields as $field) {
					$fieldset = '';
					$field_value = '';
					if(!empty($item[$field['field_name']]))
						$field_value = $item[$field['field_name']];
					if($field['field_type']=='string') {
						$fieldset .= $this->get_template_block('field_type_string');
						$fieldset = str_replace("%field_id%", $field['id'], $fieldset);
						$fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
						$fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
						$fieldset = str_replace("%field_value%", $field_value, $fieldset);
					} elseif($field['field_type']=='pagesite') {
						$fieldset .= $this->get_template_block('field_type_pagesite');
						$fieldset = str_replace("%field_id%", $field['id'], $fieldset);
						$fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
						$fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
						$fieldset = str_replace("%field_value%", $field_value, $fieldset);
					}  elseif($field['field_type']=='textarea') {
						$fieldset .= $this->get_template_block('field_type_textarea');
						$fieldset = str_replace("%field_id%", $field['id'], $fieldset);
						$fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
						$fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
						$fieldset = str_replace("%field_value%", $field_value, $fieldset);
					} elseif($field['field_type']=='select') {
						$fieldset .= $this->get_template_block('field_type_select');
						$fieldset = str_replace("%field_id%", $field['id'], $fieldset);
						$fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
						$fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
						$options = '';
						$values = explode("\n", trim($field['field_params']));
						foreach ($values as $value) {
							$option = $this->get_template_block('field_type_select_options');
							$option = str_replace("%field_value%", $value, $option);
							if(!empty($field_value)) {
								if($field_value == trim($value))
									$option = str_replace("%selected%", ' selected="selected"', $option);
								else
									$option = str_replace("%selected%", '', $option);
							} else {
								$option = str_replace("%selected%", '', $option);
							}
							$options .= $option;
						}
						$fieldset = str_replace("%options%", $options, $fieldset);
					} elseif($field['field_type']=='checkbox') {
						$fieldset .= $this->get_template_block('field_type_checkbox');
						$fieldset = str_replace("%field_id%", $field['id'], $fieldset);
						$fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
						$fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
						$options = '';
						$values = explode("\n", $field['field_params']);
						$setvalues = unserialize($field_value);
						foreach ($values as $value) {
							$option = $this->get_template_block('field_type_checkbox_options');
							$option = str_replace("%field_name%", trim($field['field_name']), $option);
							$option = str_replace("%field_value%", trim($value), $option);
							if(!empty($setvalues[trim($value)])) {
								if($setvalues[trim($value)] == 'on')
									$option = str_replace("%checked%", ' checked="checked"', $option);
								else
									$option = str_replace("%checked%", '', $option);
							} else {
								$option = str_replace("%checked%", '', $option);
							}
							$options .= $option;
						}
						$fieldset = str_replace("%options%", $options, $fieldset);
					} elseif($field['field_type']=='fileselect') {
						$fieldset .= $this->get_template_block('field_type_fileselect');
						$fieldset = str_replace("%field_id%", $field['id'], $fieldset);
						$fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
						$fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
						$fieldset = str_replace("%field_value%", $field_value, $fieldset);
					} elseif($field['field_type']=='imageselect') {
						$fieldset .= $this->get_template_block('field_type_imageselect');
						$fieldset = str_replace("%field_id%", $field['id'], $fieldset);
						$fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
						$fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
						$fieldset = str_replace("%field_value%", $field_value, $fieldset);
					} elseif($field['field_type']=='editor') {
						$fieldset .= $this->get_template_block('field_type_editor');
						$editor = new edit_content(true);
						$editor->set_edit_name($field['field_name']);
						$editor->set_simple_theme();
						$editor->set_content($field_value);
						$fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
						$fieldset = str_replace('%editor%', $editor->create(), $fieldset);
					};
					$fields_list .= $fieldset;
				}
				if(!empty($fields_list)) {
					$content_fields = $this->get_template_block('content_fields');
					$content_fields = str_replace("%fields_list%", $fields_list, $content_fields);
					$content = str_replace("%content_fields%", $content_fields, $content);
				} else {
					$content = str_replace("%content_fields%", '', $content);
				}

				$content = str_replace("%id%", $item['id'], $content);
				$content = str_replace("%blockid%", $blockid, $content);
				$content = str_replace("%order%", $item['order'], $content);
				$content = str_replace("%action%", $kernel->pub_redirect_for_form("item_save"), $content);
				break;
			case "content_fields":
				$content = $this->get_template_block('content_fields_list');
				$rows = '';
				$fields = $this->get_content_fields($moduleid);
				$field_order = 0;
				foreach ($fields as $field) {
					$row = $this->get_template_block('content_fields_row');
					$row = str_replace('%id%', $field['id'], $row);
					$row = str_replace('%field_name%', $field['field_name'], $row);
					$row = str_replace('%field_title%', $field['field_title'], $row);
					$row = str_replace('%field_type%', $field['field_type'], $row);
					$row = str_replace('%field_order%', $field_order, $row);
					$field_order = $field_order+5;
					$rows .= $row;
				}
				$content = str_replace('%action%', $kernel->pub_redirect_for_form("content_fields_save"), $content);
				$content = str_replace('%rows%', $rows, $content);
				break;
			case "content_fields_edit": // выводим форму для добавления или редактирования поля контента
				$content = $this->get_template_block('content_fields_edit');
				$id = intval($kernel->pub_httpget_get('id'));

				if($id==0) {
					$content = str_replace('%form_title%', $this->get_template_block('content_fields_title_new'), $content);
					$content = str_replace('%field_title%', '', $content);
					$content = str_replace('%field_name%', '', $content);
					$content = str_replace('%field_type%', '', $content);
					$content = str_replace('%disabled%', '', $content);
					$content = str_replace('%string_selected%', '', $content);
					$content = str_replace('%pagesite_selected%', '', $content);
					$content = str_replace('%select_selected%', '', $content);
					$content = str_replace('%textarea_selected%', '', $content);
					$content = str_replace('%checkbox_selected%', '', $content);
					$content = str_replace('%fileselect_selected%', '', $content);
					$content = str_replace('%imageselect_selected%', '', $content);
					$content = str_replace("%field_params%", '', $content);
				} else {
					$field = $this->get_content_field($moduleid, $id);
					$content = str_replace('%form_title%', $this->get_template_block('content_fields_title_edit'), $content);
					$content = str_replace('%field_title%', $field['field_title'], $content);
					$content = str_replace('%field_name%', $field['field_name'], $content);

					if($field['field_type']=='string')
						$content = str_replace('%string_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='pagesite')
						$content = str_replace('%pagesite_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='select')
						$content = str_replace('%select_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='textarea')
						$content = str_replace('%textarea_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='checkbox')
						$content = str_replace('%checkbox_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='fileselect')
						$content = str_replace('%fileselect_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='imageselect')
						$content = str_replace('%imageselect_selected%', 'selected="selected"', $content);

					$content = str_replace("%field_params%", $field['field_params'], $content);

					$content = str_replace('%disabled%', 'disabled="disabled"', $content);
					$content = str_replace('%string_selected%', '', $content);
					$content = str_replace('%pagesite_selected%', '', $content);
					$content = str_replace('%select_selected%', '', $content);
					$content = str_replace('%textarea_selected%', '', $content);
					$content = str_replace('%checkbox_selected%', '', $content);
				}

				$content = str_replace('%action%', $kernel->pub_redirect_for_form("content_fields_save"), $content);
				$content = str_replace('%id%', $id, $content);
				break;
			case "content_fields_save": // сохраняем/обновляем соотвествующее поле контента
				$id = intval($kernel->pub_httppost_get('id'));
				$save_order = $kernel->pub_httppost_get('save_order');
				$fields_delete = $kernel->pub_httppost_get('fields_delete');
				$field_title = trim($kernel->pub_httppost_get('content_field_title'));
				$field_name = trim($kernel->pub_httppost_get('content_field_name'));

				if($fields_delete) {
					$field_id = $kernel->pub_httppost_get('field_id');
					foreach($field_id as $id => $value) {
						if(is_numeric($id));
						$this->delete_content_field($moduleid, $id);
					}
					$kernel->pub_redirect_refresh_reload("content_fields");
					break;
				}

				if($save_order) {
					$field_order = $kernel->pub_httppost_get('field_order');
					foreach($field_order as $id => $order) {
						$query = 'UPDATE `'.$kernel->pub_prefix_get().'_content_fields` SET `field_order`="'.$order.'" WHERE `id`="'.$id.'" AND `module_id`="'.$moduleid.'";';
						$kernel->runSQL($query);
					}
					$kernel->pub_redirect_refresh_reload("content_fields");
					break;
				}

				if(!empty($field_name))
					$field_name = $this->priv_translate_string2db($field_name);
				else
					$field_name = $this->priv_translate_string2db($field_title);

				$field_type = $kernel->pub_httppost_get('content_field_type');
				$field_params = $kernel->pub_httppost_get('content_field_params');

				if($id==0 && !$save_order) {
					$field_name = $this->gen_field_name($moduleid, $field_name);

                    if($field_type == 'editor' || $field_type == 'textarea')
                        $query = "ALTER TABLE `".$kernel->pub_prefix_get()."_".$moduleid."` ADD `".$field_name."` TEXT NOT NULL;";
                    else
                        $query = "ALTER TABLE `".$kernel->pub_prefix_get()."_".$moduleid."` ADD `".$field_name."` VARCHAR(255) NOT NULL;";

                    if($kernel->runSQL($query)) {
						$query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_content_fields` (`module_id`, `field_type`, `field_title`, `field_name`, `field_params`, `field_order`) VALUES ("'.$moduleid.'", "'.$field_type.'", "'.$field_title.'","'.$field_name.'", "'.$field_params.'", "0");';
						$kernel->runSQL($query);
					}
				} elseif($id > 0) {
					$field = $this->get_content_field($moduleid, $id);
					$field_name_old = $field['field_name'];
					$query = 'UPDATE `'.$kernel->pub_prefix_get().'_content_fields` SET `field_type`="'.$field_type.'", `field_title`="'.$field_title.'", `field_name`="'.$field_name.'", `field_params`="'.$field_params.'" WHERE `id`="'.$id.'" AND `module_id`="'.$moduleid.'";';
					if($kernel->runSQL($query)) {
                        if($field_type == 'editor' || $field_type == 'textarea')
                            $query = "ALTER TABLE `".$kernel->pub_prefix_get()."_".$moduleid."` CHANGE `".$field_name_old."` `".$field_name."` TEXT NOT NULL;";
                        else
                            $query = "ALTER TABLE `".$kernel->pub_prefix_get()."_".$moduleid."` CHANGE `".$field_name_old."` `".$field_name."` VARCHAR(255) NOT NULL;";

                        $kernel->runSQL($query);
					}
				}

				$kernel->pub_redirect_refresh_reload("content_fields");
				break;
			case "content_fields_delete": // удаляем соотвествующее произвольное поле
				$getid = intval($kernel->pub_httpget_get('id'));
				if(!empty($getid))
					$this->delete_content_field($moduleid, $getid);

				$kernel->pub_redirect_refresh_reload("content_fields");
				break;
		}

		$content = str_replace('%submodul_name%', $this->get_current_module_name(), $content);
		return $content;
	}


	/**
	 * Возвращает блоки контента принадлежащих конкретному модулю
	 *
	 * @param string $moduleid ID-модуля
	 * @return array
	 */
	private function get_blocks($moduleid)
	{
		global $kernel;
		return $kernel->db_get_list_simple("_content_blocks", "`module_id`='".$moduleid."'");
	}

	/**
	 * Возвращает блок контента по ID блока
	 *
	 * @param string $id ID-блока контента
	 * @return array
	 */
	private function get_block($id)
	{
		global $kernel;
		return $kernel->db_get_record_simple("_content_blocks", "`id`='".$id."'");
	}


	/**
	 * Возвращает запись из блока контента по ID записи
	 *
	 * @param string $id ID-блока контента
	 * @return array
	 */
	private function get_item($id)
	{
		global $kernel;
		$moduleid = $kernel->pub_module_id_get();
		return $kernel->db_get_record_simple("_".$moduleid, "`id`='".$id."'");
	}

	/**
	 * Возвращает количество записей блока контента по ID записи конкретного модуля
	 *
	 * @param string $moduleid ID-модуля
	 * @param string $blockid ID-блока контента
	 * @return int
	 */
	private function get_items_count($moduleid, $blockid)
	{
		global $kernel;
		$total = 0;
		$crec = $kernel->db_get_record_simple("_".$moduleid, "`block_id`='".$blockid."'", "count(*) AS count");
		if ($crec)
			$total = $crec['count'];
		return $total;
	}

	/**
	 * Удаляет запись из блока контента по ID записи
	 *
	 * @param array $item ID-блока контента
	 * @return bolean
	 */
	private function item_delete($item)
	{
		global $kernel;
		$moduleid = $kernel->pub_module_id_get();
		$query = "DELETE FROM `".$kernel->pub_prefix_get()."_".$moduleid."` WHERE `id`='".$item['id']."';";
		$kernel->runSQL($query);
	}

	/**
	 * Возвращает поле блока контента по ID
	 *
	 * @param string $moduleid ID-модуля
	 * @param string $id ID-поле
	 * @return array
	 */
	private function get_content_field($moduleid, $id)
	{
		global $kernel;
		$cond = "`module_id`='".$moduleid."' AND `id`='".$id."'";
		return $kernel->db_get_record_simple("_content_fields", $cond);
	}

	/**
	 * Удаляет поле блока контента по ID
	 *
	 * @param string $moduleid ID-модуля
	 * @param string $id ID-поле
	 * @return bolean
	 */
	private function delete_content_field($moduleid, $id)
	{
		global $kernel;
		$field = $this->get_content_field($moduleid, $id);

		if(!empty($field["field_name"]))
			$kernel->runSQL('ALTER TABLE `'.$kernel->pub_prefix_get().'_'.$moduleid.'` DROP `'.$field["field_name"].'`');

		$kernel->runSQL('DELETE FROM `'.$kernel->pub_prefix_get().'_content_fields` WHERE `id`="'.$id.'" AND `module_id`="'.$moduleid.'"');
	}

	/**
	 * Возвращает доступные поля блока контента
	 *
	 * @param string $moduleid ID-модуля
	 * @return array
	 */
	private function get_content_fields($moduleid)
	{
		global $kernel;
		$cond = "`module_id`='".$moduleid."' ORDER BY `field_order` ASC";
		return $kernel->db_get_list_simple("_content_fields", $cond);
	}

	/**
	 * Генерирует, проверяет на дубли и возвращает допустимое имя поля для хранения в БД
	 *
	 * @param string $moduleid ID-модуля
	 * @param string $field_name Название поля
	 * @return string
	 */
	private function gen_field_name($moduleid, $field_name)
	{
		global $kernel;
		$newfield_name = $field_name;
		$cond = "`field_name` = '".$newfield_name."'";
		$fields = $kernel->db_get_list_simple("_content_fields", $cond);
		$res = $kernel->runSQL("SHOW COLUMNS FROM `".$kernel->pub_prefix_get()."_".$moduleid."` WHERE `Field` = '".$newfield_name."'");
		if(count($fields)>0 || count($res)>0) {
			for($i=1;$i<=10;$i++) {
				$res = $kernel->runSQL("SHOW COLUMNS FROM `".$kernel->pub_prefix_get()."_".$moduleid."` WHERE `Field` = '".$newfield_name.$i."'");
				if(count($res->fetch_assoc())==0) {
					$newfield_name = $newfield_name.$i;
					break;
				}
			}
		}
		return $newfield_name;
	}

	/**
	 * Производит поиск и замену меток на значения записи из блока контента в зависимости от типа значения (ENUM, SET, etc.)
	 *
	 * @param array $fields Массив полей контента
	 * @param array $data Массив значений полей контента
	 * @param string $content Шаблон для вывода блока контента
	 * @return string
	 */
	private function priv_content_fields($fields, $data, $content)
	{
		global $kernel;
		
		if ($kernel->pub_https_scheme_get())
			$host = 'https://'.$kernel->pub_http_host_get();
		else
			$host = 'http://'.$kernel->pub_http_host_get();
		
		foreach($fields as $field) {
			if(!empty($data[$field['field_name']])) {
				if($field['field_type']=='checkbox') {
					
					$setvalue = unserialize($data[$field['field_name']]);
					$curr_field = $this->get_template_block($field['field_name']);
					if(!empty($setvalue)) {
						$curr_field_vals = '';
						$curr_field_val = $this->get_template_block($field['field_name'].'_val');
						
						foreach($setvalue as $key=>$value) {
							if($value=='on')
								$curr_field_vals .= str_replace('%setvalue%', $key, $curr_field_val);
						}
						$curr_field_val = $curr_field_vals;
						
					} else {
						$curr_field_val = $this->get_template_block($field['field_name'].'_null');
					}
					
					$curr_field = str_replace('%'.$field['field_name'].'_value%', $curr_field_val, $curr_field);
					$content = str_replace('%'.$field['field_name'].'%', $curr_field, $content);
					
				} else {
					
					if($field['field_type'] == 'textarea')
						$value = nl2br($data[$field['field_name']]);
					elseif($field['field_type'] == 'pagesite')
						$value = $host.'/'.$data[$field['field_name']];
					else
						$value = $data[$field['field_name']];
					
					$curr_field = $this->get_template_block($field['field_name']);
					if(!empty($curr_field)) {
						$curr_field = str_replace('%'.$field['field_name'].'_value%', $value, $curr_field);
						$content = str_replace('%'.$field['field_name'].'%', $curr_field, $content);
					} else {
						$content = str_replace('%'.$field['field_name'].'%', $value, $content);
					}
					
					$content = str_replace('%'.$field['field_name'].'_value%', $value, $content);
					
				}
			} else {
				
				$curr_field_null = $this->get_template_block($field['field_name'].'_null');
				if(!empty($curr_field_null))
					$content = str_replace('%'.$field['field_name'].'%', $curr_field_null, $content);
				else
					$content = str_replace('%'.$field['field_name'].'%', '', $content);
				
				$content = str_replace('%'.$field['field_name'].'_value%', '', $content);
			}
		}
		return $content;
	}

	/**
	 * Производит транслитерацию значения имени поля
	 *
	 * @param string $res имя поля для хранения в БД
	 * @return string
	 */
	private function priv_translate_string2db($res)
	{
		global $kernel;
		$res = $kernel->pub_translit_string($res);
		$res = preg_replace("/[^0-9a-z_]/i", '', $res);
		return strtolower($res);
	}

	/**
	 * Возвращает список доступных блоков контента для выбора в публичном действии
	 *
	 * @return array
	 */
	public static function get_blocks_select()
	{
		global $kernel;
		$moduleid = $kernel->pub_module_id_get();
		$list = array("null" => "[#content_block_not_set#]");
		$blocks = $kernel->db_get_list_simple("_content_blocks", "`module_id`='".$moduleid."'");
		foreach($blocks as $item) {
			$list[$item['id']] = $item['name'];
		}
		return $list;
	}
}