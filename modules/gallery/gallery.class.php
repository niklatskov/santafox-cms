<?php
/**
 * Модуль "Галерея"
 *
 * @author Alehandr aleks-konsultant@mail.ru, s@nchez s@nchez.me
 * @name gallery
 * @contributors Oslix <oslix@yandex.ru>, Alexsander Vyshnyvetskyy (alex_wdmg) <wdmg.com.ua@gmail.com>, bubek <bvvtut@tut.by>, paranoik.via@gmail.com <paranoik.via@gmail.com>, sanchez <sanchezby@gmail.com>, rinatr <mail.rinat@yandex.ru>, sanchez <s@nchez.me>, paranoik-via <paranoik.via@gmail.com>, Alexsander Vyshnyvetskyy <wdmg.com.ua@gmail.com>, Rinat <mail.rinat@yandex.ru>, VIA <ivan@vbsite.ru>, bubek <bubek@iva-lawer.bre.ls>, Alexander Merkulov <sasha@merqlove.ru>
 * @version 3.0
 *
 */

require_once dirname(dirname(dirname(__FILE__)))."/include/basemodule.class.php";

class gallery  extends basemodule
{
	private $offset_param_name="go";
	private $cat_param_name="gcat";

	// Выводит список категорий пользователю
	function pub_categories_list($template_list, $gallery_page='', $ifcounter=true) {
		global $kernel;
		
		$moduleid = $kernel->pub_module_id_get();
		$this->set_templates($kernel->pub_template_parse($template_list));
        $fields = $this->get_custom_fields($moduleid, 'category');

		$items = array();
		$query = "SELECT cats.*, photos.image AS photoimage FROM ".$kernel->pub_prefix_get()."_gallery_cats AS cats
                      LEFT JOIN ".$kernel->pub_prefix_get()."_".$moduleid." AS photos ON photos.cat_id=cats.id
                      WHERE cats.module_id='".$moduleid."' GROUP BY cats.id";
		
		$res = $kernel->runSQL($query);
		while ($row = mysqli_fetch_assoc($res)) {
			$items[] = $row;
		}
		mysqli_free_result( $res);
		$content = $this->get_template_block('categories_content');

		$catid = intval($kernel->pub_httpget_get($this->cat_param_name));
		
		$curr_cat = false;
		if ($catid > 0)
			$curr_cat = $this->get_category($catid);

		if (count($items) == 0)
			return $this->get_template_block('no_data');

		if(empty($gallery_page))
			$gallery_page = $kernel->pub_page_current_get();

		if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
			$curr_page = '/'.$gallery_page;
		else
			$curr_page = $gallery_page.".html";

		if (!$curr_cat)
			$def_link = $this->get_template_block('categories_all_rows_active');
		else
			$def_link = $this->get_template_block('categories_all_rows');
		
		if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
			$def_link = str_replace('%link%', $curr_page."/", $def_link);
		else
			$def_link = str_replace('%link%', $curr_page, $def_link);
		
		if($ifcounter) {
			$count = $this->get_images_count(false);

			if($count > 0)
				$counter = $this->get_template_block('categories_counter');
			else
				$counter = $this->get_template_block('categories_counter_null');

			$counter = str_replace('%count%', $count, $counter);
			$def_link = str_replace('%counter%', $counter, $def_link);
		} else {
			$def_link = str_replace('%counter%', '', $def_link);
		}
		
		$lines = '';
		foreach($items as $data) {

			if($curr_cat['id']==$data['id'])
				$line = $this->get_template_block('categories_rows_active');
			else
				$line = $this->get_template_block('categories_rows');

			$line = str_replace('%name%', $data['name'], $line);
			$line = str_replace('%title%', $data['title'], $line);
			$line = str_replace('%catid%', $data['id'], $line);

			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL && $curr_page == '/index') {
				$line = str_replace('%link%', "/".$kernel->pub_pretty_url($data['name']).'-'.$this->cat_param_name.$data['id'].'/', $line);
            } else {
				if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
					$line = str_replace('%link%', $curr_page."/".$kernel->pub_pretty_url($data['name']).'-'.$this->cat_param_name.$data['id'].'/', $line);
				else
					$line = str_replace('%link%', $curr_page."?".$this->cat_param_name."=".$data['id'], $line);
            }
            
			if(isset($data['thumbnail']) && !empty($data['thumbnail'])) {
				$line = str_replace('%small_image%', "/content/images/".$moduleid."/tn/".$data['thumbnail'], $line);
				$line = str_replace('%source_image%', "/content/images/".$moduleid."/source/".$data['thumbnail'], $line);
				$line = str_replace('%value_image%', "/content/images/".$moduleid."/".$data['thumbnail'], $line);
			} else if(isset($data['photoimage']) && !empty($data['photoimage'])) {
				$line = str_replace('%small_image%', "/content/images/".$moduleid."/tn/".$data['photoimage'], $line);
				$line = str_replace('%source_image%', "/content/images/".$moduleid."/source/".$data['photoimage'], $line);
				$line = str_replace('%value_image%', "/content/images/".$moduleid."/".$data['photoimage'], $line);
			} else {
				$line = str_replace('%small_image%', "", $line);
				$line = str_replace('%source_image%', "", $line);
				$line = str_replace('%value_image%', "", $line);
			}
			$line = str_replace('%category_description%', str_replace('%category_description%',$data['description'],$this->get_template_block('category_description')), $line);

            foreach ($fields as $field) {
                $fieldset = '';
                if (!empty($data[$field['field_name']])) {
                    $field_value = $data[$field['field_name']];
                    $fieldset = str_replace('%' . $field['field_name'] . '_value%', $field_value, $this->get_template_block($field['field_name']));
                } else {
                    $fieldset = $this->get_template_block($field['field_name']."_null");
                }
                $line = str_replace('%'.$field['field_name'].'%', $fieldset, $line);
            }

			if($ifcounter) {
				$count = $this->get_images_count($data['id']);

				if($count > 0)
					$counter = $this->get_template_block('categories_counter');
				else
					$counter = $this->get_template_block('categories_counter_null');

				$counter = str_replace('%count%', $count, $counter);
				$line = str_replace('%counter%', $counter, $line);
			} else {
				$line = str_replace('%counter%', '', $line);
			}
			
			$lines .= $line;
		}

		$content = str_replace('%rows%', $def_link.$lines, $content);
		return $content;

	}

	// Показываем содержимое галереи пользователю
	function pub_create_content($template, $perpage, $sortParam="asc", $gallery_page='', $max_pages = 5)
	{
		global $kernel;

		$perpage = intval($perpage);

		if ($perpage < 1)
			$perpage = 20;

		$offset = intval($kernel->pub_httpget_get($this->offset_param_name));
		$page =  intval($kernel->pub_httpget_get('page'));

		if((empty($offset) && !empty($page)) && (defined("USE_PRETTY_URL") && USE_PRETTY_URL))
			$offset = $perpage * ($page-1);

		$catid = intval($kernel->pub_httpget_get($this->cat_param_name));
		$total = 0;
		$curr_cat = false;
		$moduleid = $kernel->pub_module_id_get();
		$this->set_templates($kernel->pub_template_parse($template));

		if ($this->get_categories_count($moduleid) == 0 || $catid > 0)
		{ //если нету категорий или указана категория - выводим фотки
			$show_cats = false;
			
			$cond = "`available`='1'";
			if ($catid > 0)
				$cond .= " AND `cat_id`='".$catid."'";
			
			// Изменение сортировки
			switch ($sortParam) {
				case 'asc':
					$cond.=" ORDER BY `id` ASC";
					break;
				case 'desc':
					$cond.=" ORDER BY `id` DESC";
					break;
				case 'orderAsc':
					$cond.=" ORDER BY `order` ASC";
					break;
				case 'orderDesc':
					$cond.=" ORDER BY `order` DESC";
					break;
				default:
					$cond.=" ORDER BY `id` ASC";
					break;
			}
			
			$items = $kernel->db_get_list_simple("_".$moduleid, $cond, "*", $offset, $perpage);
			$crec = $kernel->db_get_record_simple("_".$moduleid, $cond, "count(*) AS count");

			if ($crec)
				$total = $crec['count'];
			
			$content = $this->get_template_block('content');
			
			if ($catid > 0)
				$curr_cat = $this->get_category($catid);
			
		}
		else
		{ //иначе - список категорий
			$show_cats = true;
			$items = array();
			$query = "SELECT cats.*, photos.image AS photoimage FROM ".$kernel->pub_prefix_get()."_gallery_cats AS cats
                      LEFT JOIN ".$kernel->pub_prefix_get()."_".$moduleid." AS photos ON photos.cat_id=cats.id
                      WHERE cats.module_id='".$moduleid."' GROUP BY cats.id";
			$res=$kernel->runSQL($query);
			while ($row=mysqli_fetch_assoc($res))
			{
				$items[]=$row;
			}
			mysqli_free_result( $res);
			$content = $this->get_template_block('categories_content');
		}

		if (count($items)==0)
			return $this->get_template_block('no_data');

		if(empty($gallery_page))
			$gallery_page = $kernel->pub_page_current_get();

		if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
			$curr_page = '/'.$gallery_page;
		else
			$curr_page = $gallery_page.".html";

		$lines = '';
		$fields = $this->get_custom_fields($moduleid);
		foreach($items as $data)
		{
			if ($show_cats)
			{
				$line = $this->get_template_block('categories_rows');
				$line = str_replace('%name%', $data['name'], $line);
				$line = str_replace('%title%', $data['title'], $line);
				$line = str_replace('%catid%', $data['id'], $line);

				if (defined("USE_PRETTY_URL") && USE_PRETTY_URL && $curr_page == '/index')
					$line = str_replace('%link%', "/".$kernel->pub_pretty_url($data['name']).'-'.$this->cat_param_name.$data['id'].'/', $line);if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
				if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
					$line = str_replace('%link%', $curr_page."/".$kernel->pub_pretty_url($data['name']).'-'.$this->cat_param_name.$data['id'].'/', $line);
				else
					$line = str_replace('%link%', $curr_page."?".$this->cat_param_name."=".$data['id'], $line);

                if (isset($data['thumbnail']) && !empty($data['thumbnail'])) {
                    $line = str_replace('%small_image%', "/content/images/".$moduleid."/tn/".$data['thumbnail'], $line);
                    $line = str_replace('%source_image%', "/content/images/".$moduleid."/source/".$data['thumbnail'], $line);
                    $line = str_replace('%value_image%', "/content/images/".$moduleid."/".$data['thumbnail'], $line);
                } else if (isset($data['photoimage']) && !empty($data['photoimage'])) {
                    $line = str_replace('%small_image%', "/content/images/".$moduleid."/tn/".$data['photoimage'], $line);
                    $line = str_replace('%source_image%', "/content/images/".$moduleid."/source/".$data['photoimage'], $line);
                    $line = str_replace('%value_image%', "/content/images/".$moduleid."/".$data['photoimage'], $line);
                } else {
                    $line = str_replace('%small_image%', "", $line);
                    $line = str_replace('%source_image%', "", $line);
                    $line = str_replace('%value_image%', "", $line);
                }

				$line = str_replace('%category_description%', str_replace('%category_description%',$data['description'],$this->get_template_block('category_description')), $line);
			}
			else
			{
				$line = $this->get_template_block('rows');
				$line = $this->priv_custom_fields($fields, $data, $line);
				$line = str_replace('%link_small_image%',"/content/images/".$moduleid."/tn/".$data['image'], $line);
				$line = str_replace('%link_big_image%',"/content/images/".$moduleid."/".$data['image'], $line);
				$line = str_replace('%link_source_image%',"/content/images/".$moduleid."/source/".$data['image'], $line);
				$line = str_replace('%name_image%',  $data['name_image'], $line);
				$line = str_replace('%title_image%',  $data['title_image'], $line);
				$line = str_replace('%order%',  $data['order'], $line);
				$line = str_replace('%description%', $data['description'], $line);
				$line = str_replace('%post_date%',   $data['post_date'], $line);
			}
			$lines .= $line;
		}

		$content = str_replace('%rows%', $lines, $content);
		if (!$show_cats)
		{
			// Сформируем ссылки пагинации
			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
				$purl = $curr_page."/";
				if ($curr_cat) {
					$purl .= $kernel->pub_pretty_url($curr_cat['name'])."-".$this->cat_param_name.$curr_cat['id'].'/';
				}
				$purl .= '/page-';
			} else {
				$purl = $curr_page."?";
				if ($curr_cat) {
					$purl .= $this->cat_param_name.'='.$curr_cat['id'].'&';
				}
				$purl .= $this->offset_param_name."=";
			}

			$content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $perpage, $purl, $max_pages), $content);
			if ($curr_cat)
			{
				$content = str_replace('%category_id%', $curr_cat['id'], $content);
				$content = str_replace('%category_name%', str_replace('%category_name%',$curr_cat['name'],$this->get_template_block('category_name')), $content);
				$content = str_replace('%category_descr%', str_replace('%category_descr%',$curr_cat['description'],$this->get_template_block('category_descr')), $content);

				// Добавим ссылку возврата к категориям галереи
				if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
					$content = str_replace('%back2cats_link%', str_replace('%link%', $curr_page, $this->get_template_block('back2cats_link')), $content);
				else
					$content = str_replace('%back2cats_link%', str_replace('%link%', $curr_page, $this->get_template_block('back2cats_link')), $content);

				//Добавим раздел в дорогу сайта
				if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
					$kernel->pub_waysite_set(array('url' => '/'.$curr_page."/", 'caption' => $curr_cat['name']));
				else
					$kernel->pub_waysite_set(array('url' => $curr_page.".html", 'caption' => $curr_cat['name']));

				//Добавим в тайтл раздел
				$kernel->pub_page_title_add($curr_cat['name']);
			}
			else
			{
				$content = str_replace('%category_id%', '', $content);
				$content = str_replace('%category_name%', '', $content);
				$content = str_replace('%category_descr%', '', $content);
				$content = str_replace('%back2cats_link%', '', $content);
			}
		}

		return $content;
	}

	/**
	 * Функция для вывода фотографий категории (с учетом сортировки)
	 *
	 * @param string $template шаблон
	 * @param integer $max
	 * @param string $sortParam
	 * @param string $cat_id категория, если указана
     * @param integer $max_pages Страниц в блоке
	 * @return string
	 */
	function pub_show_photos_sort($template, $max, $sortParam='asc', $cat_id='', $max_pages = 5)
	{
		global $kernel;
		$max = intval($max);
		$total = 0;
		$all_cat = false;
		$curr_cat = false;

		$offset = intval($kernel->pub_httpget_get($this->offset_param_name));
		$page =  intval($kernel->pub_httpget_get('page'));

		if((empty($offset) && !empty($page)) && (defined("USE_PRETTY_URL") && USE_PRETTY_URL))
			$offset = $max * ($page-1);
		
		if ($max<1)
			$max=5;
		
		$moduleid = $kernel->pub_module_id_get();
		$this->set_templates($kernel->pub_template_parse($template));
        $fields = $this->get_custom_fields($moduleid, 'category');
		$cond = "`available`='1'";

		if (empty($cat_id) && $kernel->pub_httpget_get($this->cat_param_name, false, false))
			$cat_id = intval($kernel->pub_httpget_get($this->cat_param_name));
			
		if (!empty($cat_id)) {
			$curr_cat = $this->get_category($cat_id);
			$cond .= " AND cat_id='".$cat_id."'";
		} else {
            $all_cat = true;
        }
        
		if(empty($gallery_page))
			$gallery_page = $kernel->pub_page_current_get();

		if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
			$curr_page = '/'.$gallery_page;
		else
			$curr_page = $gallery_page.".html";

		switch ($sortParam) {
			case 'asc':
				$cond.=" ORDER BY `id` ASC";
				break;
			case 'desc':
				$cond.=" ORDER BY `id` DESC";
				break;
			case 'orderAsc':
				$cond.=" ORDER BY `order` ASC";
				break;
			case 'orderDesc':
				$cond.=" ORDER BY `order` DESC";
				break;
			default:
				$cond.=" ORDER BY `id` ASC";
				break;
		}


		$items = $kernel->db_get_list_simple("_".$moduleid, $cond, "*", $offset, $max);
		if (count($items)==0)
			return $this->get_template_block('no_data');

		$crec = $kernel->db_get_record_simple("_".$moduleid, $cond, "count(*) AS count");
		if ($crec)
			$total = $crec['count'];
		
		$lines = '';
		$fields = $this->get_custom_fields($moduleid);
		foreach($items as $data)
		{
			$line = $this->get_template_block('rows');
			$line = $this->priv_custom_fields($fields, $data, $line);
			$line = str_replace('%link_small_image%',"/content/images/".$moduleid."/tn/".$data['image'], $line);
			$line = str_replace('%link_big_image%',"/content/images/".$moduleid."/".$data['image'], $line);
			$line = str_replace('%link_source_image%',"/content/images/".$moduleid."/source/".$data['image'], $line);
			$line = str_replace('%name_image%',  $data['name_image'], $line);
			$line = str_replace('%title_image%',  $data['title_image'], $line);
			$line = str_replace('%order%',  $data['order'], $line);
			$line = str_replace('%description%', $data['description'], $line);
			$line = str_replace('%post_date%',   $data['post_date'], $line);
			$line = str_replace('%cat_id%',   $data['cat_id'], $line);
			$lines .= $line;
		}
		$content = $this->get_template_block('content');
		$content = str_replace('%rows%', $lines, $content);
		//если используется тот же шаблон, что и для вывода галереи, уберём метку для постранички, название категории, и ссылку на список категорий
		
		
		if($curr_cat) {
			// Сформируем ссылки пагинации для текущей категории
			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
				$purl = $curr_page."/";
				if ($curr_cat) {
					$purl .= $kernel->pub_pretty_url($curr_cat['name'])."-".$this->cat_param_name.$curr_cat['id'].'/';
				}
				$purl .= '/page-';
			} else {
				$purl = $curr_page."?";
				if ($curr_cat) {
					$purl .= $this->cat_param_name.'='.$curr_cat['id'].'&';
				}
				$purl .= $this->offset_param_name."=";
			}
			$content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $max, $purl, $max_pages), $content);
            $content = str_replace('%category_id%', $curr_cat['id'], $content);
			$content = str_replace('%category_name%', str_replace('%category_name%',$curr_cat['name'],$this->get_template_block('category_name')), $content);
			$content = str_replace('%category_descr%', str_replace('%category_description%',$curr_cat['description'],$this->get_template_block('category_description')), $content);

            foreach ($fields as $field) {
                if (!empty($curr_cat[$field['field_name']])) {
                    $field_value = $curr_cat[$field['field_name']];
                    $fieldset = str_replace('%category_' . $field['field_name'] . '%', $field_value, $this->get_template_block('category_'.$field['field_name']));
                } else {
                    $fieldset = $this->get_template_block('category_'.$field['field_name']."_null");
                }
                $content = str_replace('%category_'.$field['field_name'].'%', $fieldset, $content);
            }

            //Добавим раздел в дорогу сайта
            if (defined("USE_PRETTY_URL") && USE_PRETTY_URL)
                $kernel->pub_waysite_set(array('url' => $kernel->pub_pretty_url($curr_cat['name'])."-".$this->cat_param_name.$curr_cat['id'].'/', 'caption' => $curr_cat['name']));
            else
                $kernel->pub_waysite_set(array('url' => $curr_page.".html", 'caption' => $curr_cat['name']));
            
            //Добавим в тайтл раздел
            $kernel->pub_page_title_add($curr_cat['name']);
            
		} else if($all_cat) {
			// Сформируем ссылки пагинации для всех категорий
			if (defined("USE_PRETTY_URL") && USE_PRETTY_URL) {
				$purl = $curr_page."/";
				if ($curr_cat) {
					$purl .= '/';
				}
				$purl .= '/page-';
			} else {
				$purl = $curr_page."?";
				if ($curr_cat) {
					$purl .= '?';
				}
				$purl .= $this->offset_param_name."=";
			}
			$content = str_replace('%pages%', $this->build_pages_nav($total, $offset, $max, $purl, $max_pages), $content);
		} else {
			$content = str_replace('%pages%', '', $content);
		}
		
		$content = str_replace('%category_id%', '', $content);
		$content = str_replace('%category_name%', '', $content);
		$content = str_replace('%category_descr%', '', $content);
		return $content;
	}

	/**
	 * Функция для вывода случайных фотографий
	 *
	 * @param string $template шаблон
	 * @param integer $max
	 * @return string
	 */
	function pub_random_photos($template, $max)
	{
		global $kernel;
		$max = intval($max);
		
		if ($max<1)
			$max=5;
		
		$moduleid = $kernel->pub_module_id_get();
		$this->set_templates($kernel->pub_template_parse($template));
		$cond="`available`='1' ORDER BY RAND()";
		
		$items = $kernel->db_get_list_simple("_".$moduleid, $cond, "*", 0, $max);
		if (count($items)==0)
			return $this->get_template_block('no_data');

		$lines = '';
		$fields = $this->get_custom_fields($moduleid);
		foreach($items as $data)
		{
			$line = $this->get_template_block('rows');
			$line = $this->priv_custom_fields($fields, $data, $line);
			$line = str_replace('%link_small_image%',"/content/images/".$moduleid."/tn/".$data['image'], $line);
			$line = str_replace('%link_big_image%',"/content/images/".$moduleid."/".$data['image'], $line);
			$line = str_replace('%link_source_image%',"/content/images/".$moduleid."/source/".$data['image'], $line);
            $line = str_replace('%name_image%',  $data['name_image'], $line);
			$line = str_replace('%title_image%',  $data['title_image'], $line);
			$line = str_replace('%order%',  $data['order'], $line);
			$line = str_replace('%description%', $data['description'], $line);
			$line = str_replace('%post_date%',   $data['post_date'], $line);
			$line = str_replace('%cat_id%',   $data['cat_id'], $line);
			$lines .= $line;
		}
		$content = $this->get_template_block('content');
		$content = str_replace('%rows%', $lines, $content);
		//если используется тот же шаблон, что и для вывода галереи, уберём метку для постранички, название категории, и ссылку на список категорий
		$content = str_replace('%pages%', '', $content);
		$content = str_replace('%category_id%', '', $content);
		$content = str_replace('%category_name%', '', $content);
		$content = str_replace('%category_descr%', '', $content);
		return $content;
	}
    
	/**
	 * Публичное действие для оотбражения `title` категории
	 *
	 * @param string $html_title_def Заголовок категории по умолчанию
	 * @return string
	 */
    public function pub_show_html_title($html_title_def) {
        global $kernel;
        $html_title = '';
		$catid = intval($kernel->pub_httpget_get($this->cat_param_name));
		if (!empty($catid) && $catid > 0) {
            $currcat = $this->get_category($catid);
            $html_title = $currcat['name'];
        }
        
        if($html_title != '')
            return $html_title;
        else
            return trim($html_title_def);
    }
    
	/**
	 * Публичное действие для отображения `meta-keywords` категории
	 *
	 * @param string $meta_keywords_def Ключевые слова категории по умолчанию
	 * @return string
	 */
    public function pub_show_meta_keywords($meta_keywords_def) {
        global $kernel;
        $meta_keywords = '';
		$catid = intval($kernel->pub_httpget_get($this->cat_param_name));
		if (!empty($catid) && $catid > 0) {
            $currcat = $this->get_category($catid);
            $meta_keywords = $currcat['meta_keywords'];
        }
        
        if($meta_keywords != '')
            return $meta_keywords;
        else
            return trim($meta_keywords_def);
    }
    
	/**
	 * Публичное действие для оотбражения `meta-description` категории
	 *
	 * @param string $meta_description_def Описание категории по умолчанию
	 * @return string
	 */
    public function pub_show_meta_description($meta_description_def) {
        global $kernel;
        $meta_description = '';
		$catid = intval($kernel->pub_httpget_get($this->cat_param_name));
		if (!empty($catid) && $catid > 0) {
            $currcat = $this->get_category($catid);
            $meta_description = $currcat['meta_description'];
        }
        
        if($meta_description != '')
            return $meta_description;
        else
            return trim($meta_description_def);
    }
    
	/**
	 * Функция для построения меню для административного интерфейса
	 *
	 * @param pub_interface $menu Обьект класса для управления построением меню
	 * @return boolean true
	 */
	function interface_get_menu($menu)
	{
		$menu->set_menu_block('[#gallery_module_label_block_menu#]');
		$menu->set_menu("[#gallery_menu_photos#]","show_photos");
		$menu->set_menu("[#gallery_menu_add#]","image_edit&image_id=0");
		$menu->set_menu("[#gallery_menu_cats#]","show_cats");
		$menu->set_menu("[#gallery_import_archive#]","import_archive_form");
		$menu->set_menu("[#gallery_menu_custom_fields#]","custom_fields");
		$menu->set_menu_default('show_photos');
		return true;
	}

	function get_categories_count($moduleid)
	{
		global $kernel;
		$total = 0;
		
		$crec = $kernel->db_get_record_simple("_gallery_cats", "`module_id`='".$moduleid."'", "count(*) AS count");
		
		if ($crec)
			$total = $crec['count'];
		
		return $total;
	}
	
	function get_images_count($cat_id)
	{
		global $kernel;
		$moduleid = $kernel->pub_module_id_get();
		$total = 0;
		
		if($cat_id)
			$crec = $kernel->db_get_record_simple("_".$moduleid, "`cat_id`='".$cat_id."' AND `available`='1'", "count(*) AS count");
		else
			$crec = $kernel->db_get_record_simple("_".$moduleid, "`available`='1'", "count(*) AS count");
		
		if ($crec)
			$total = $crec['count'];
		
		return $total;
	}
	
	function get_categories($moduleid)
	{
		global $kernel;
		return $kernel->db_get_list_simple("_gallery_cats", "`module_id`='".$moduleid."'");
	}

	function get_category($id)
	{
		global $kernel;
		return $kernel->db_get_record_simple("_gallery_cats", "id=".$id);
	}

	function get_image($id)
	{
		global $kernel;
		$moduleid = $kernel->pub_module_id_get();
		return $kernel->db_get_record_simple("_".$moduleid, "id=".$id);
	}

	static function image_delete($irec)
	{
		global $kernel;
		$moduleid = $kernel->pub_module_id_get();
		$query = "DELETE FROM `".$kernel->pub_prefix_get()."_".$moduleid."` WHERE `id`='".$irec['id']."';";
		$kernel->runSQL($query);
		if (!empty($irec['image']))
		{
			$kernel->pub_file_delete('content/images/'.$moduleid.'/tn/'.$irec['image'], false);
			$kernel->pub_file_delete('content/images/'.$moduleid.'/source/'.$irec['image'], false);
			$kernel->pub_file_delete('content/images/'.$moduleid.'/'.$irec['image'], false);
		}
	}

	private function process_file_upload($file,$dir)
	{
		global $kernel;

		if (!is_uploaded_file($file['tmp_name']))
			return false;

		//Имя файла пропустим через транслит, что бы исключить руские буквы
		//отделив сначала расширение
		$file_ext = pathinfo($file['name'], PATHINFO_EXTENSION);
		$filename = basename($file['name'], ".".$file_ext);
		$only_name = $kernel->pub_translit_string($filename);
		$filename = $only_name.".".$file_ext;

		//Проверим наличе дубликата, и добавим цифру если что
		$i = 0;
		while (file_exists($dir.$filename))
		{
			$filename = $only_name.'_'.$i.'.'.$file_ext;//$i."_".$filename;
			$i++;
		}

		$kernel->pub_file_move($file['tmp_name'], $dir.$filename, true, true);
		return $dir.$filename;
	}

	function process_image($image_path)
	{
		global $kernel;
		$moduleid = $kernel->pub_module_id_get();
		$img_source_width    = $kernel->pub_modul_properties_get('img_source_width');
		$img_source_height   = $kernel->pub_modul_properties_get('img_source_height');
		$img_big_width    = $kernel->pub_modul_properties_get('img_big_width');
		$img_big_height   = $kernel->pub_modul_properties_get('img_big_height');
		$img_small_width  = $kernel->pub_modul_properties_get('img_small_width');
		$img_small_height = $kernel->pub_modul_properties_get('img_small_height');

		$watermark_path   = $kernel->pub_modul_properties_get('path_to_copyright_file');
		$watermark_place  = $kernel->pub_modul_properties_get('copyright_position');
		$watermark_transparency = $kernel->pub_modul_properties_get('copyright_transparency');

		$source_image = array('width' => $img_source_width['value'],
							  'height' => $img_source_height['value']);
		$big_image = array('width' => $img_big_width['value'],
						   'height' => $img_big_height['value']);
		$thumb_image = array('width' => $img_small_width['value'],
							 'height' => $img_small_height['value']);

		if(intval($kernel->pub_httppost_get('copyright'))>0)
			$watermark_image = array('path' => $watermark_path['value'],
									 'place' => $watermark_place['value'],
									 'transparency' => $watermark_transparency['value']);
		else
			$watermark_image = 0;
		
		$path_to_save = 'content/images/'.$moduleid;
		$filename = $kernel->pub_image_save($image_path, 'img'.rand(1000,9999), $path_to_save, $big_image, $thumb_image, $watermark_image, $source_image, $watermark_image);
		return $filename;
	}

	function start_admin()
	{
		global $kernel;
		$content = '';
		$select_menu = $kernel->pub_section_leftmenu_get();
		$moduleid = $kernel->pub_module_id_get();
		$this->set_templates($kernel->pub_template_parse("modules/gallery/templates_admin/template_admin.html"));
		switch ($select_menu)
		{
			case "import_archive_form":
				$msg = '';
				$sessionMsg = "123";
				$content = $this->get_template_block('import_form');

				if (isset($_POST['do_archive_import']))
				{
					$deleteArchive=false;
					if ($kernel->pub_httppost_get('import_from')=="upload_file")
					{
						$archive_file = $this->process_file_upload($_FILES['archive'],$kernel->pub_site_root_get()."/upload/");
						if (!$archive_file)
							$sessionMsg="[#gallery_import_upload_failed#]";
						else
							$deleteArchive=true;
					}
					else
						$archive_file = $kernel->pub_httppost_get('file_on_server');

					if ($archive_file)
					{
						set_time_limit(180);
						ini_set('max_execution_time', 180);


						require_once(dirname(dirname(dirname(__FILE__)))."/components/pclzip/pclzip.lib.php");

						$archive = new PclZip($archive_file);
						$list = $archive->listContent();
						$added=0;
						if ($list == 0) //ошибка чтения архива
							$sessionMsg="[#gallery_import_archive_read_failed#]";
						else
						{
							$sessionMsg = 'file: '.preg_replace('~(.+)/([^/]+)~','$2',$archive_file);
							//$sessionMsg = 'file: '.$archive_file;
							$cat_id = $kernel->pub_httppost_get('cat_id');
							$order = $kernel->pub_httppost_get('order');
							foreach ($list as $file_index=>$afileRec)
							{
								$afile = $afileRec['filename'];
                                
                                // Пропуск __MACOSX директории создаваемой OS X архиватором
                                if ('__MACOSX/' === substr($afile, 0, 9))
									continue;
                                
								//нам нужны только изображения
								if (!preg_match('~\.(gif|jpg|png|jpeg)$~i',$afile))
									continue;
                                
								$eres = $archive->extract(PCLZIP_OPT_BY_INDEX, array($file_index), PCLZIP_OPT_PATH,$kernel->pub_site_root_get()."/upload/",PCLZIP_OPT_REMOVE_ALL_PATH);
								if ($eres == 0)
								{//ошибка распаковки
									$sessionMsg.=" [#gallery_import_archive_read_failed#] (".$afile.") ";
									continue;
								}
								$filenameOrig=preg_replace('~(.+)/([^/]+)~','$2',$afile);
								$file_path = $kernel->pub_site_root_get()."/upload/".$filenameOrig;
								if (file_exists($file_path))
								{
									$filename=$this->process_image($file_path);
									$order=($file_index+1);
									$kernel->pub_file_delete($file_path, false);
									if ($kernel->pub_httppost_get('gen_names')=='filenames')
										$title_image=$kernel->pub_translit_string(preg_replace('~\.(gif|jpg|png|jpeg)~i','',$filenameOrig));
									else
										$title_image=$order;

									$query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_'.$moduleid.'` (`cat_id`, `description`, `name_image`, `title_image`, `image`, `order`, `post_date`, `available`) VALUES ('.$cat_id.', "", "'.$title_image.'", "'.$title_image.'", "'.$filename.'", "'.$order.'", "'.date("Y-m-d").'", "1");';
									$kernel->runSQL($query);
									$added++;

								}
							}
							$sessionMsg.=', [#gallery_import_added#]'.$added;
						}
						if ($deleteArchive)
							$kernel->pub_file_delete($archive_file, false);
					}
					$kernel->pub_session_set('import_msg', $sessionMsg);
					$kernel->pub_redirect_refresh_reload('import_archive_form');
					exit;

				}
				$cats_lines='';
				$cats = $this->get_categories($moduleid);
				foreach ($cats as $cat)
				{
					$cats_lines.="<option value='".$cat['id']."'>".$cat['name']."</option>";
				}
				$files_lines = '';
				$files = array_keys($kernel->pub_files_list_get($kernel->pub_site_root_get()."/upload"));
				foreach ($files as $file)
				{
					if (preg_match('~\.zip$~i', $file))
					{
						$files_lines.='<option value="'.$file.'">'.preg_replace('~(.+)/([^/]+)~','$2',$file).'</option>';
					}
				}

				$content = str_replace('%action%', $kernel->pub_redirect_for_form('import_archive_form'), $content);
				$content = str_replace("%max_upload_size%", ini_get('post_max_size'), $content);
				$content = str_replace("%catlines%", $cats_lines, $content);
				$content = str_replace("%server_files%", $files_lines, $content);
				$smsg = $kernel->pub_session_get('import_msg');
				if (!is_null($smsg))
				{
					$msg .= $smsg;
					$kernel->pub_session_unset('import_msg');
				}
				$content = str_replace("%msg%",$msg, $content);
				break;
			case "category_delete":
				$catid=intval($kernel->pub_httpget_get('id'));
				$kernel->runSQL("UPDATE `".$kernel->pub_prefix_get()."_".$moduleid."` SET `cat_id`=0 WHERE `cat_id`='".$catid."'");
				$kernel->runSQL("DELETE FROM `".$kernel->pub_prefix_get()."_gallery_cats` WHERE id=".$catid);
				$kernel->pub_redirect_refresh('show_cats');
				break;
			case "category_edit":
				$cat = $this->get_category(intval($kernel->pub_httpget_get('catid')));
				if (!$cat) {
                    $category_thumbnail = "";
					$cat = array('id'=>0, 'name'=>'', 'title'=>'', 'thumbnail'=>'', 'meta_keywords'=>'', 'meta_description'=>'');
                } else {
                    $category_thumbnail = $this->get_template_block('category_thumbnail');
                }
                $content = $this->get_template_block('category_form');
				$editor = new edit_content();
				$editor->set_edit_name('description');
				$editor->set_simple_theme(true);
				
                if (isset($cat['description']) && !empty($cat['description']))
					$editor->set_content($cat['description']);
				else
					$editor->set_content('');
                
                if (isset($cat['thumbnail']) && !empty($cat['thumbnail']))
					$category_thumbnail = str_replace('%img_src%', '/content/images/'.$cat['module_id'].'/tn/'.$cat['thumbnail'], $category_thumbnail);
				else
					$category_thumbnail = "";
                
				$content = str_replace('%thumbnail%', $category_thumbnail, $content);
				$content = str_replace('%description%', $editor->create(), $content);

                $content = $this->custom_fields_replace('%custom_fields%', $cat, $content, 'category');

                $content = str_replace('%action%', $kernel->pub_redirect_for_form('category_save'), $content);
				foreach ($cat as $k=>$v)
				{
					$content = str_replace('%'.$k.'%', htmlspecialchars($v), $content);
				}
				break;
			case "category_save":
				$id = intval($kernel->pub_httppost_get('id'));
				$name = $kernel->pub_httppost_get('name');
				$title = $kernel->pub_httppost_get('title');
				$descr = $kernel->pub_httppost_get('description');
				$meta_keywords = $kernel->pub_httppost_get('meta_keywords');
				$meta_description = $kernel->pub_httppost_get('meta_description');

                $fields = $this->get_custom_fields($moduleid, 'category');

				if ($id==0) {

                    $field_names = '';
                    $field_values = '';

                    foreach ($fields as $field) {
                        $custom_field = $kernel->pub_httppost_get($field['field_name']);
                        $field_names .= ', `'.$field['field_name'].'`';
                        if(!empty($custom_field)) {
                            if($field['field_type']=='checkbox')
                                $field_values .= ", '".serialize($custom_field)."'";
                            else
                                $field_values .= ', "'.$custom_field.'"';
                        } else {
                            $field_values .= ', ""';
                        }
                    }

                    if (is_uploaded_file($_FILES['file_image']['tmp_name']))
                        $thumbnail = $this->process_image($_FILES['file_image']['tmp_name']);
                    else
                        $thumbnail = false;

                    $query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_gallery_cats` (`name`,`title`,`thumbnail`,`module_id`,`description`,`meta_keywords`,`meta_description`'.$field_names.') VALUES ("'.$name.'", "'.$title.'", "'.$thumbnail.'", "'.$moduleid.'", "'.$descr.'", "'.$meta_keywords.'", "'.$meta_description.'"'.$field_values.');';
				} else {

                    $field_values = '';
                    foreach ($fields as $field) {
                        $custom_field = $kernel->pub_httppost_get($field['field_name']);
                        if(!empty($custom_field)) {
                            if($field['field_type']=='checkbox')
                                $field_values .= ", `".$field['field_name']."` = '".serialize(array_map('trim', $custom_field))."'";
                            else
                                $field_values .= ', `'.$field['field_name'].'` = "'.$custom_field.'"';
                        } else {
                            $field_values .= ', `'.$field['field_name'].'` = ""';
                        }
                    }
                    
					$query = "UPDATE `".$kernel->pub_prefix_get()."_gallery_cats` SET `description`='".$descr."',`name`='".$name."',`title`='".$title."',`meta_keywords`='".$meta_keywords."',`meta_description`='".$meta_description."'".$field_values;
                    
					if (is_uploaded_file($_FILES['file_image']['tmp_name']))
						$thumbnail = $this->process_image($_FILES['file_image']['tmp_name']);
					else
                        $thumbnail = false;
                    
                    if ($thumbnail && !empty($thumbnail)) {
                        $query .= ",`thumbnail`='".$thumbnail."'";
                        $irec = $kernel->db_get_record_simple("_gallery_cats", "id='".$id."'");
                        if ($irec['thumbnail'] && !empty($irec['thumbnail'])) {
                            $kernel->pub_file_delete('content/images/'.$irec['module_id'].'/tn/'.$irec['thumbnail'], false);
                            $kernel->pub_file_delete('content/images/'.$irec['module_id'].'/source/'.$irec['thumbnail'], false);
                            $kernel->pub_file_delete('content/images/'.$irec['module_id'].'/'.$irec['thumbnail'], false);
                        }
                    }
					$query .= " WHERE id=".$id;
                }
				$kernel->runSQL($query);
				$kernel->pub_redirect_refresh_reload('show_cats');
				break;
			case "show_cats":
				$content = $this->get_template_block('categories_list');
				//$cats = $this->get_categories($moduleid);
                $query = "SELECT cats.*, photos.image AS photoimage FROM ".$kernel->pub_prefix_get()."_gallery_cats AS cats
                LEFT JOIN ".$kernel->pub_prefix_get()."_".$moduleid." AS photos ON photos.cat_id=cats.id
                WHERE cats.module_id='".$moduleid."' GROUP BY cats.id";
                $cats = $kernel->runSQL($query);
				$rows = '';
				foreach ($cats as $cat)
				{
                    $count = $this->get_images_count($cat['id']);
					$row = $this->get_template_block('categories_row');
					$row = str_replace('%id%', $cat['id'], $row);
					$row = str_replace('%name%', $cat['name'], $row);
					$row = str_replace('%title%', $cat['title'], $row);
                    
                    $thumbnail = $this->get_template_block('categories_thumbnail');
                    if(isset($cat['thumbnail']) && !empty($cat['thumbnail'])) {
                        $thumbnail = str_replace('%image%', '/content/images/'.$moduleid.'/tn/'.$cat['thumbnail'], $thumbnail);
                    } else if(isset($cat['photoimage']) && !empty($cat['photoimage'])) {
                        $thumbnail = str_replace('%image%', '/content/images/'.$moduleid.'/tn/'.$cat['photoimage'], $thumbnail);
                    } else {
                        $thumbnail = $this->get_template_block('categories_thumbnail_null');
                    }
                    $row = str_replace('%thumbnail%', $thumbnail, $row);
                    
					$row = str_replace('%counter%', $count, $row);
					$rows .= $row;
				}
				$content = str_replace('%rows%', $rows, $content);
				break;
			case "image_save":
				$id = intval($kernel->pub_httppost_get('id'));
                $name_image = $kernel->pub_httppost_get('name_image');
				$title_image = $kernel->pub_httppost_get('title_image');
				$cat_id = intval($kernel->pub_httppost_get('cat_id'));
				$order = intval($kernel->pub_httppost_get('order'));
				$available = intval($kernel->pub_httppost_get('available'));
				$descr = $kernel->pub_httppost_get('koment_image');

				$fields = $this->get_custom_fields($moduleid);

				if ($id==0)//обработка изображения только при новой фото
				{
					$field_names = '';
					$field_values = '';

					foreach ($fields as $field) {
						$custom_field = $kernel->pub_httppost_get($field['field_name']);
						$field_names .= ', `'.$field['field_name'].'`';
						if(!empty($custom_field)) {
							if($field['field_type']=='checkbox')
								$field_values .= ", '".serialize($custom_field)."'";
							else
								$field_values .= ', "'.$custom_field.'"';
						} else {
                            $field_values .= ', ""';
						}
					}
                    
					if (!is_uploaded_file($_FILES['file_image']['tmp_name'])) {
						$kernel->pub_redirect_refresh_reload("show_photos");
						return "";
					}

					$tmp_file_image = $_FILES['file_image']['tmp_name'];
					$filename = $this->process_image($tmp_file_image);
					$query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_'.$moduleid.'` (`cat_id`, `description`, `name_image`, `title_image`, `image`, `order`, `available`, `post_date`'.$field_names.') VALUES ('.$cat_id.', "'.$descr.'", "'.$name_image.'", "'.$title_image.'", "'.$filename.'", "'.$order.'", "1", "'.date("Y-m-d").'"'.$field_values.');';
				}
				else
				{
					$field_values = '';
					foreach ($fields as $field) {
						$custom_field = $kernel->pub_httppost_get($field['field_name']);
						if(!empty($custom_field)) {
							if($field['field_type']=='checkbox')
								$field_values .= ", `".$field['field_name']."` = '".serialize(array_map('trim', $custom_field))."'";
							else
								$field_values .= ', `'.$field['field_name'].'` = "'.$custom_field.'"';
						} else {
                            $field_values .= ', `'.$field['field_name'].'` = ""';
						}
					}
                    
					$filename = false;
					if (is_uploaded_file($_FILES['file_image']['tmp_name']))
					{
						$tmp_file_image = $_FILES['file_image']['tmp_name'];
						$filename = $this->process_image($tmp_file_image);
					}

					$query = 'UPDATE `'.$kernel->pub_prefix_get().'_'.$moduleid.'` SET `cat_id`='.$cat_id.', `description`="'.$descr.'", `name_image`="'.$name_image.'", `title_image`="'.$title_image.'", `order`="'.$order.'", `available`="'.$available.'"'.$field_values;
					if ($filename && !empty($filename)) {
						$query .= ' ,`image` = "'.$filename.'"';
						// Удаляем изображение
						$irec=$kernel->db_get_record_simple("_".$moduleid, "id='".$id."'");
						$kernel->pub_file_delete('content/images/'.$irec['module_id'].'/tn/'.$irec['image'], false);
						$kernel->pub_file_delete('content/images/'.$irec['module_id'].'/source/'.$irec['image'], false);
						$kernel->pub_file_delete('content/images/'.$irec['module_id'].'/'.$irec['image'], false);
					}
					$query .= ' WHERE id='.$id;
				}

				$kernel->runSQL($query);
				$kernel->pub_redirect_refresh_reload("show_photos");
				break;

			case "image_delete":
				$irec=$kernel->db_get_record_simple("_".$moduleid, "`id`='".intval($kernel->pub_httpget_get('image_id'))."'");
				if ($irec)
					self::image_delete($irec);
                
				$kernel->pub_redirect_refresh('show_photos');
				break;

			//список фотографий
			case 'show_photos':
				$perpage = 50;
				$total = 0;
				$offset = intval($kernel->pub_httpget_get('offset'));

				$cond = "true";
				$catid = intval($kernel->pub_httpget_get('catid'));
				if ($catid>0)
					$cond.=" AND cat_id=".$catid;
				
				$sortby = $kernel->pub_httpget_get('sortby');
				switch ($sortby)
				{
					case 'id':
					default:
						$cond.=" ORDER BY id";
						break;
                    case 'name':
                        $cond.=" ORDER BY `name_image`";
                        break;
					case 'title':
						$cond.=" ORDER BY `title_image`";
						break;
					case 'date':
						$cond.=" ORDER BY `post_date`";
						break;
					case 'sort':
						$cond.=" ORDER BY `order`";
						break;
				}


				$images = $kernel->db_get_list_simple("_".$moduleid, $cond);
				$cats = $this->get_categories($moduleid);
				$crec = $kernel->db_get_record_simple("_".$moduleid, $cond, "count(*) AS count");
				if ($crec)
					$total=$crec['count'];
				$lines='';
				foreach ($images as $data)
				{
					$line = $this->get_template_block('line');
					$line = str_replace("%id%",   		  $data['id'],        		$line);
                    $line = str_replace("%name_image%",  $data['name_image'],     $line);
					$line = str_replace("%title_image%",  $data['title_image'],     $line);

					$description = strip_tags($data['description']);
					$description = substr($description, 0, 480);
					$description = rtrim($description, "!,.-");
					$description = substr($description, 0, strrpos($description, ' '))."…";
					$line = str_replace("%description%",  $description,     $line);
					
					$available = $this->get_template_block('not_available');
					if(intval($data['available']) == 1)
						$available = $this->get_template_block('available');
					
					$line = str_replace("%available%",  $available, $line);
					
					$line = str_replace("%order%",  		  $data['order'],     $line);
					$line = str_replace("%link_image%",   "/content/images/".$moduleid."/tn/".$data['image'],	$line);
					$line = str_replace("%post_date%",    $data['post_date'],       $line);
					$lines .= $line;
				}
				$content = $this->get_template_block('begin').$lines.$this->get_template_block('end');

				$cats_lines='';
				foreach ($cats as $cat)
				{
					if ($catid==$cat['id'])
						$cats_lines.="<option value='".$cat['id']."' selected>".$cat['name']."</option>";
					else
						$cats_lines.="<option value='".$cat['id']."'>".$cat['name']."</option>";
				}
				$content = str_replace('%cid%', $catid, $content);
				$content = str_replace("%sortby%", $sortby, $content);
				$content = str_replace("%catlines%", $cats_lines, $content);
				$content = str_replace("%total%", $total, $content);
				$purl='show_photos&catid='.$catid.'&sortby='.$sortby.'&offset=';
				$content = str_replace('%pages%', $this->build_pages_nav($total,$offset,$perpage,$purl,0,'url'), $content);
				$content = str_replace("%action%", $kernel->pub_redirect_for_form("batch_prep"), $content);

				//Добавляем категории
				$allCats = $kernel->db_get_list_simple('_gallery_cats', 'true ORDER BY `name` ASC');
				$rows = '';
				foreach ( $allCats AS $cat ) {
					$row = str_replace('%catName%', $cat['name'], $this->get_template_block('allCat'));
					$row = str_replace('%catId%', $cat['id'], $row);
					$rows .= $row;
				}
				$content = str_replace('%allCat%', $rows, $content);


				break;

			case "batch_prep":
				//Смотрим, что пришло
				$post = $kernel->pub_httppost_get();
				$allPict = false;
				// Если нет действия, выходим
				if( !isset($post['action']) || !isset($post['cid']) )
					$kernel->pub_redirect_refresh('show_photos');

				if (isset($post['allPict'])) {
					$allPict = true;
				}
				
				$items = array();
				if(isset($post['item']))
					$items = array_keys($post['item']);

				if ($post['action'] != 'delete') {
					
					// Обновляем записи об изображениях
					$cond = 'true';
					
					if ($post['action'] == 'available_on')
						$rec = array('available' => '1');
					elseif ($post['action'] == 'available_off')
						$rec = array('available' => '0');
					else
						$rec = array('cat_id' => intval($post['action']));

					if ($post['cid'])
						$cond = "`cat_id` = '".$post['cid']."'";

					if(count($items) > 0) {
						if (!$allPict) //Только выбранные фото
							$cond = "`id` IN (".implode(",", $items).")";
					}

					$kernel->db_update_record("_".$moduleid, $rec, $cond);
				} else {
					// Удаляем записи об изображениях
					if ($allPict) {
						// все в категории
						if ($post['cid'])
							$cond = "`cat_id` = '".$post['cid']."'";
                        else
                            $cond = "true";
                        
						$delitems = $kernel->db_get_list_simple("_".$moduleid, $cond);
					} else {
						// только выделенные
						if(count($items) > 0) {
							$cond = "`id` IN (".implode(",", $items).")";
							$delitems = $kernel->db_get_list_simple("_".$moduleid, $cond);
						}
					}
					
					//Удаляем сами изображения
					if($delitems) {
						foreach ($delitems AS $irec) {
							self::image_delete($irec);
						}
					}
					
				}
				//$kernel->debug($post);
				$kernel->pub_redirect_refresh_reload("show_photos");
				break;

			//выводим форму для добавления или редактирования картинки
			case "image_edit":
				$id = intval($kernel->pub_httpget_get('image_id'));
				$image = $this->get_image($id);
				if (!$image) {
					$cond = "true";
					$catid = intval($kernel->pub_httpget_get('catid'));
					if ($catid>0)
						$cond.=" AND `cat_id`='".$catid."'";
					
					$maxid = $kernel->db_get_record_simple("_".$moduleid, $cond, "MAX(`order`) as maxid");
                    $image = array('id'=>0, 'name_image'=>'', 'title_image'=>'', 'order'=>($maxid ? $maxid['maxid']+5 : 1), 'description'=>'', 'image'=>'', 'cat_id'=>0);
				}

				if ($image['id']==0)
					$content = $this->get_template_block('form_add');
				else
					$content = $this->get_template_block('form_edit');
				
				if ($image['id']==0)
					$image['available'] = 1;
				
				$cats_lines='';
				$cats = $this->get_categories($moduleid);
				foreach ($cats as $cat)
				{
					if ($image['cat_id']==$cat['id'])
						$cats_lines.="<option value='".$cat['id']."' selected>".$cat['name']."</option>";
					else
						$cats_lines.="<option value='".$cat['id']."'>".$cat['name']."</option>";
				}

                $content = $this->custom_fields_replace('%custom_fields%', $image, $content, 'image');

				$content = str_replace("%catlines%", $cats_lines, $content);
				$content = str_replace("%img_src%", "/content/images/".$moduleid."/tn/".$image['image'], $content);
				$content = str_replace("%id%", $image['id'], $content);
				$content = str_replace("%order%", $image['order'], $content);
				
				if(intval($image['available']) == 1)
					$content = str_replace("%available_checked%", 'checked="checked"', $content);
				else
					$content = str_replace("%available_checked%", '', $content);
				
                $content = str_replace("%name_image%", htmlspecialchars($image['name_image']), $content);
				$content = str_replace("%title_image%", htmlspecialchars($image['title_image']), $content);
				$content = str_replace("%action%", $kernel->pub_redirect_for_form("image_save"), $content);
				$editor = new edit_content(true);
				$editor->set_edit_name('koment_image');
				$editor->set_simple_theme();
				$editor->set_content($image['description']);
				$content = str_replace('%editor%', $editor->create(), $content);
				break;
			case "custom_fields": // выводим список произвольных полей
				$content = $this->get_template_block('custom_fields_list');
				$rows = '';
				$fields = $this->get_custom_fields($moduleid);
				$field_order = 0;
				foreach ($fields as $field) {
					$row = $this->get_template_block('custom_fields_row');
					$row = str_replace('%id%', $field['id'], $row);
					$row = str_replace('%field_name%', $field['field_name'], $row);
					$row = str_replace('%field_title%', $field['field_title'], $row);
					$row = str_replace('%field_type%', $field['field_type'], $row);
					$row = str_replace('%field_section%', $field['field_section'], $row);
					$row = str_replace('%field_order%', $field_order, $row);
					$field_order = $field_order+5;
					$rows .= $row;
				}
				$content = str_replace('%action%', $kernel->pub_redirect_for_form("custom_fields_save"), $content);
				$content = str_replace('%rows%', $rows, $content);
				break;
			case "custom_fields_edit": // выводим форму для добавления или редактирования произвольного поля
				$content = $this->get_template_block('custom_fields_edit');
				$id = intval($kernel->pub_httpget_get('id'));

				if($id==0) {
					$content = str_replace('%form_title%', $this->get_template_block('custom_fields_title_new'), $content);
					$content = str_replace('%field_title%', '', $content);
					$content = str_replace('%field_name%', '', $content);
					$content = str_replace('%field_type%', '', $content);
					$content = str_replace('%disabled%', '', $content);
					$content = str_replace('%string_selected%', '', $content);
					$content = str_replace('%pagesite_selected%', '', $content);
					$content = str_replace('%select_selected%', '', $content);
					$content = str_replace('%textarea_selected%', '', $content);
					$content = str_replace('%checkbox_selected%', '', $content);
					$content = str_replace('%date_selected%', '', $content);
					$content = str_replace('%fileselect_selected%', '', $content);
					$content = str_replace('%imageselect_selected%', '', $content);
                    $content = str_replace('%field_section%', '', $content);
					$content = str_replace('%image_selected%', '', $content);
					$content = str_replace('%category_selected%', '', $content);
					$content = str_replace('%field_section_disabled%', '', $content);
					$content = str_replace("%field_params%", '', $content);
				} else {
					$field = $this->get_custom_field($moduleid, $id);
					$content = str_replace('%form_title%', $this->get_template_block('custom_fields_title_edit'), $content);
					$content = str_replace('%field_title%', $field['field_title'], $content);
					$content = str_replace('%field_name%', $field['field_name'], $content);

					if($field['field_type']=='string')
						$content = str_replace('%string_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='pagesite')
						$content = str_replace('%pagesite_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='select')
						$content = str_replace('%select_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='textarea')
						$content = str_replace('%textarea_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='checkbox')
						$content = str_replace('%checkbox_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='date')
						$content = str_replace('%date_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='fileselect')
						$content = str_replace('%fileselect_selected%', 'selected="selected"', $content);
					elseif($field['field_type']=='imageselect')
						$content = str_replace('%imageselect_selected%', 'selected="selected"', $content);

					if($field['field_section']=='image')
						$content = str_replace('%image_selected%', 'selected="selected"', $content);
					elseif($field['field_section']=='category')
						$content = str_replace('%category_selected%', 'selected="selected"', $content);

                    $content = str_replace('%field_section_disabled%', ' disabled="disabled"', $content);

					$content = str_replace("%field_params%", $field['field_params'], $content);

					$content = str_replace('%disabled%', 'disabled="disabled"', $content);
					$content = str_replace('%string_selected%', '', $content);
					$content = str_replace('%pagesite_selected%', '', $content);
					$content = str_replace('%select_selected%', '', $content);
					$content = str_replace('%textarea_selected%', '', $content);
					$content = str_replace('%checkbox_selected%', '', $content);
				}

				$content = str_replace('%action%', $kernel->pub_redirect_for_form("custom_fields_save"), $content);
				$content = str_replace('%id%', $id, $content);
				break;
			case "custom_fields_save": // сохраняем/обновляем соотвествующее произвольное поле
				$id = intval($kernel->pub_httppost_get('id'));
				$save_order = $kernel->pub_httppost_get('save_order');
				$fields_delete = $kernel->pub_httppost_get('fields_delete');
				$field_title = trim($kernel->pub_httppost_get('gallery_field_title'));
				$field_name = trim($kernel->pub_httppost_get('gallery_field_name'));
                $field_type = $kernel->pub_httppost_get('gallery_field_type');
                $field_section = $kernel->pub_httppost_get('gallery_field_section');
                $field_params = $kernel->pub_httppost_get('gallery_field_params');

                if (!$field_type || empty($field_type))
                    $field_type = 'string';

                if (!$field_section || empty($field_section))
                    $field_section = 'image';

				if ($fields_delete) {
					$field_id = $kernel->pub_httppost_get('field_id');
					foreach($field_id as $id => $value) {
						if(is_numeric($id));
						$this->dele_custom_field($moduleid, $id);
					}
					$kernel->pub_redirect_refresh_reload("custom_fields");
					break;
				}

				if ($save_order) {
					$field_order = $kernel->pub_httppost_get('field_order');
					foreach($field_order as $id => $order) {
						$query = 'UPDATE `'.$kernel->pub_prefix_get().'_gallery_fields` SET `field_order`="'.$order.'" WHERE `id`="'.$id.'" AND `module_id`="'.$moduleid.'";';
						$kernel->runSQL($query);
					}
					$kernel->pub_redirect_refresh_reload("custom_fields");
					break;
				}

				if (!empty($field_name))
					$field_name = $this->translate_string2db($field_name);
				else
					$field_name = $this->translate_string2db($field_title);

                if ($field_type == 'textarea')
                    $field_options = ' text default NULL;';
                else if ($field_type == 'date')
                    $field_options = ' date default NULL;';
                else
                    $field_options = ' varchar(255) NOT NULL;';

                $field = $this->get_custom_field($moduleid, $id);
                $field_name_old = $field['field_name'];
                $field_section_old = $field['field_section'];

				if ($id==0 && !$save_order) {
					$field_name = $this->gen_field_name($moduleid, $field_name);

                    if ($field_section == 'image') {
                        $query = "ALTER TABLE `".$kernel->pub_prefix_get()."_".$moduleid."` ADD `".$field_name."`".$field_options;
                    } else if ($field_section == 'category') {
                        $query = "ALTER TABLE `" . $kernel->pub_prefix_get() . "_gallery_cats` ADD `" . $field_name . "`".$field_options;
                    }
					if ($kernel->runSQL($query)) {
						$query = 'INSERT INTO `'.$kernel->pub_prefix_get().'_gallery_fields` (`module_id`, `field_type`, `field_section`, `field_title`, `field_name`, `field_params`, `field_order`) VALUES ("'.$moduleid.'", "'.$field_type.'", "'.$field_section.'", "'.$field_title.'","'.$field_name.'", "'.$field_params.'", "0");';
						$kernel->runSQL($query);
					}

				} else if ($id > 0) {

					if($field_section_old !== $field_section) {
                        if ($field_section_old == 'image') {
                            $query_del = "ALTER TABLE `" . $kernel->pub_prefix_get() . "_" . $moduleid . "` DELETE `" . $field_name_old . "`;";
                            $kernel->runSQL($query_del);
                        } else if ($field_section_old == 'category') {
                            $query_del = "ALTER TABLE `" . $kernel->pub_prefix_get() . "_gallery_cats` DELETE `" . $field_name_old . "`;";
                            $kernel->runSQL($query_del);
                        }
                    }

					$query = 'UPDATE `'.$kernel->pub_prefix_get().'_gallery_fields` SET `field_type`="'.$field_type.'", `field_section`="'.$field_section.'", `field_title`="'.$field_title.'", `field_name`="'.$field_name.'", `field_params`="'.$field_params.'" WHERE `id`="'.$id.'" AND `module_id`="'.$moduleid.'";';
					if ($kernel->runSQL($query)) {
                        if ($field_section == 'image') {
                            $query = "ALTER TABLE `" . $kernel->pub_prefix_get() . "_" . $moduleid . "` CHANGE `" . $field_name_old . "` `" . $field_name . "`".$field_options;
                            $kernel->runSQL($query);
                        } else if ($field_section == 'category') {
                            $query = "ALTER TABLE `" . $kernel->pub_prefix_get() . "_gallery_cats` CHANGE `" . $field_name_old . "` `" . $field_name . "`".$field_options;
                            $kernel->runSQL($query);
                        }
					}
				}

				$kernel->pub_redirect_refresh_reload("custom_fields");
				break;
			case "custom_fields_delete": // удаляем соотвествующее произвольное поле
				$getid = intval($kernel->pub_httpget_get('id'));
				if(!empty($getid))
					$this->dele_custom_field($moduleid, $getid);

				$kernel->pub_redirect_refresh_reload("custom_fields");
				break;
		}

		return $content;
	}

	private function get_custom_field($moduleid, $id)
	{
		global $kernel;
		$cond = "module_id = '".$moduleid."' AND id = '".$id."'";
		return $kernel->db_get_record_simple("_gallery_fields", $cond);
	}

	private function dele_custom_field($moduleid, $id)
	{
		global $kernel;
		$field = $this->get_custom_field($moduleid, $id);

		if(!empty($field["field_name"]))
			$kernel->runSQL('ALTER TABLE `'.$kernel->pub_prefix_get().'_'.$moduleid.'` DROP `'.$field["field_name"].'`');

		$kernel->runSQL('DELETE FROM `'.$kernel->pub_prefix_get().'_gallery_fields` WHERE `id`="'.$id.'" AND `module_id`="'.$moduleid.'"');
	}

	private function get_custom_fields($moduleid, $section = false)
	{
		global $kernel;

		if($section)
		    $cond = "`module_id`='".$moduleid."' AND `field_section`='".$section."' ORDER BY `field_order` ASC";
		else
            $cond = "`module_id`='".$moduleid."' ORDER BY `field_order` ASC";

		return $kernel->db_get_list_simple("_gallery_fields", $cond);
	}

	private function gen_field_name($moduleid, $field_name)
	{
		global $kernel;
		$newfield_name = $field_name;
		$cond = "`field_name` = '".$newfield_name."'";
		$fields = $kernel->db_get_list_simple("_gallery_fields", $cond);
		$res = $kernel->runSQL("SHOW COLUMNS FROM `".$kernel->pub_prefix_get()."_".$moduleid."` WHERE `Field` = '".$newfield_name."'");
		if(count($fields)>0 || count($res)>0) {
			for($i=1;$i<=10;$i++) {
				$res = $kernel->runSQL("SHOW COLUMNS FROM `".$kernel->pub_prefix_get()."_".$moduleid."` WHERE `Field` = '".$newfield_name.$i."'");
				if(count($res->fetch_assoc())==0) {
					$newfield_name = $newfield_name.$i;
					break;
				}
			}
		}
		return $newfield_name;
	}

	private function custom_fields_replace($replace, $values, $content, $section = 'image')
	{
        global $kernel;
        $moduleid = $kernel->pub_module_id_get();
        $fields_list = '';
        $fields = $this->get_custom_fields($moduleid, $section);
        foreach ($fields as $field) {
            $fieldset = '';
            $field_value = '';
            if(!empty($values[$field['field_name']]))
                $field_value = $values[$field['field_name']];

            if($field['field_type']=='string') {
                $fieldset .= $this->get_template_block('field_type_string');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            } elseif($field['field_type']=='pagesite') {
                $fieldset .= $this->get_template_block('field_type_pagesite');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            } elseif($field['field_type']=='textarea') {
                $fieldset .= $this->get_template_block('field_type_textarea');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            } elseif($field['field_type']=='select') {
                $fieldset .= $this->get_template_block('field_type_select');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $options = '';
                $values = explode("\n", trim($field['field_params']));
                foreach ($values as $value) {
                    $option = $this->get_template_block('field_type_select_options');
                    $option = str_replace("%field_value%", $value, $option);
                    if(!empty($field_value)) {
                        if($field_value == trim($value))
                            $option = str_replace("%selected%", ' selected="selected"', $option);
                        else
                            $option = str_replace("%selected%", '', $option);
                    } else {
                        $option = str_replace("%selected%", '', $option);
                    }
                    $options .= $option;
                }
                $fieldset = str_replace("%options%", $options, $fieldset);
            } elseif($field['field_type']=='checkbox') {
                $fieldset .= $this->get_template_block('field_type_checkbox');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $options = '';
                $values = explode("\n", $field['field_params']);
                $setvalues = unserialize($field_value);
                foreach ($values as $value) {
                    $option = $this->get_template_block('field_type_checkbox_options');
                    $option = str_replace("%field_name%", trim($field['field_name']), $option);
                    $option = str_replace("%field_value%", trim($value), $option);
                    if(!empty($setvalues[trim($value)])) {
                        if($setvalues[trim($value)] == 'on')
                            $option = str_replace("%checked%", ' checked="checked"', $option);
                        else
                            $option = str_replace("%checked%", '', $option);
                    } else {
                        $option = str_replace("%checked%", '', $option);
                    }
                    $options .= $option;
                }
                $fieldset = str_replace("%options%", $options, $fieldset);
            } elseif($field['field_type']=='date') {
                $fieldset .= $this->get_template_block('field_type_date');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            } elseif($field['field_type']=='fileselect') {
                $fieldset .= $this->get_template_block('field_type_fileselect');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            } elseif($field['field_type']=='imageselect') {
                $fieldset .= $this->get_template_block('field_type_imageselect');
                $fieldset = str_replace("%field_id%", $field['id'], $fieldset);
                $fieldset = str_replace("%field_title%", $field['field_title'], $fieldset);
                $fieldset = str_replace("%field_name%", $field['field_name'], $fieldset);
                $fieldset = str_replace("%field_value%", $field_value, $fieldset);
            };
            $fields_list .= $fieldset;
        }
        if(!empty($fields_list)) {
            $custom_fields = $this->get_template_block('custom_fields');
            $custom_fields = str_replace("%fields_list%", $fields_list, $custom_fields);
            $content = str_replace($replace, $custom_fields, $content);
        } else {
            $content = str_replace($replace, '', $content);
        }
        return $content;
	}

	private function priv_custom_fields($fields, $data, $content)
	{
		global $kernel;
		if ($kernel->pub_https_scheme_get())
			$host = 'https://'.$kernel->pub_http_host_get();
		else
			$host = 'http://'.$kernel->pub_http_host_get();
		
		foreach($fields as $field) {
			if(!empty($data[$field['field_name']])) {
				if($field['field_type']=='checkbox') {
					$setvalue = unserialize($data[$field['field_name']]);
					$custom_field = $this->get_template_block($field['field_name']);
					if(!empty($setvalue)) {
						$custom_field_vals = '';
						$custom_field_val = $this->get_template_block($field['field_name'].'_val');
						foreach($setvalue as $key=>$value) {
							if($value=='on')
								$custom_field_vals .= str_replace('%setvalue%', $key, $custom_field_val);
						}
						$custom_field_val = $custom_field_vals;
					} else {
						$custom_field_val = $this->get_template_block($field['field_name'].'_null');
					}
					$custom_field = str_replace('%'.$field['field_name'].'_value%', $custom_field_val, $custom_field);
					$content = str_replace('%'.$field['field_name'].'%', $custom_field, $content);
				} else {
					if($field['field_type']=='pagesite') {
						$custom_field = $this->get_template_block($field['field_name']);
						if(!empty($custom_field)) {
							$custom_field = str_replace('%'.$field['field_name'].'_value%', $host.'/'.$data[$field['field_name']], $custom_field);
							$content = str_replace('%'.$field['field_name'].'%', $custom_field, $content);
						} else {
							$content = str_replace('%'.$field['field_name'].'%', $host.'/'.$data[$field['field_name']], $content);
						}
					} else {
						$custom_field = $this->get_template_block($field['field_name']);
						if(!empty($custom_field)) {
							$custom_field = str_replace('%'.$field['field_name'].'_value%', $data[$field['field_name']], $custom_field);
							$content = str_replace('%'.$field['field_name'].'%', $custom_field, $content);
						} else {
							$content = str_replace('%'.$field['field_name'].'%', $data[$field['field_name']], $content);
						}
					}
				}
			} else {
				$custom_field_null = $this->get_template_block($field['field_name'].'_null');
				if(!empty($custom_field_null))
					$content = str_replace('%'.$field['field_name'].'%', $custom_field_null, $content);
				else
					$content = str_replace('%'.$field['field_name'].'%', '', $content);
			}
		}
		return $content;
	}

	private function translate_string2db($res)
	{
		global $kernel;
		$res = $kernel->pub_translit_string($res);
		$res = preg_replace("/[^0-9a-z_]/i", '', $res);
		return strtolower($res);
	}

	public static function get_categories_select()
	{
		global $kernel;
		$array  = array("" => "Все");
		$cats =  $kernel->db_get_list_simple("_gallery_cats",'true');
		foreach($cats as $item)
		{
			$array[$item['id']] = $item['name'];
		}
		return $array;
	}

}