<?php

$type_langauge = 'en';

$il['news_base_name'] = 'News';
$il['newsi_modul_base_name1'] = 'Main news';

$il['news_property_img_source_width'] = 'The width of the original image';
$il['news_property_img_source_height'] = 'The height of the original image';
$il['news_property_img_big_width'] = 'The width of the largest image in pixels';
$il['news_property_img_big_height'] = 'The height of the big picture in pixels';
$il['news_property_img_small_width'] = 'The width of the small picture in pixels';
$il['news_property_img_small_height'] = 'The height of the small picture in pixels';
$il['news_property_deliver'] = 'The value of attribute "The newsletter"';
$il['news_property_lenta'] = 'The value of attribute "in the film"';
$il['news_property_rss'] = 'The value of attribute "The RSS"';
$il['news_property_news_per_page'] = 'News page for AI';
$il['news_property_page_for_lenta'] = 'Page where the archive is formed (to be completed in the case to the news module address other newswires)';
$il['news_property_copyright_transparency'] = 'Transparent watermark';

$il['news_pub_show_lenta'] = 'Form a band';
$il['news_pub_show_lenta_template'] = 'Template Tape';
$il['news_pub_show_lenta_limit'] = 'Number of news in the tape';
$il['news_pub_show_lenta_type'] = 'Type selection';
$il['news_pub_show_lenta_type_default'] = 'Default';
$il['news_pub_show_lenta_type_past'] = 'From current date to the past';
$il['news_pub_show_lenta_type_future'] = 'From the current date into the future';
$il['news_pub_show_lenta_page'] = 'Page where the archive is formed';
$il['news_pub_show_lenta_id_modules'] = 'Module ID (comma), whose news as well get into the tape';
$il['news_pub_show_lenta_header_list']  = 'News feed header';

$il['news_property_watermark'] = 'Whether to put a watermark?';
$il['news_property_path_to_copyright_file'] = 'Select a watermark';
$il['news_property_copyright_position'] = 'Location watermark';
$il['news_property_copyright_position_0'] = 'In the center';
$il['news_property_copyright_position_1'] = 'Top-Left';
$il['news_property_copyright_position_2'] = 'Top-Right';
$il['news_property_copyright_position_3'] = 'Bottom-Right';
$il['news_property_copyright_position_4'] = 'Bottom-Left';

$il['news_pub_show_archive'] = 'Generate archive';
$il['news_pub_show_archive_template'] = 'Template';
$il['news_pub_show_archive_limit'] = 'Max on the news page';
$il['news_pub_show_archive_type'] = 'Type selection';
$il['news_pub_show_archive_default'] = 'Regardless of the choice';
$il['news_pub_show_archive_past'] = 'From current date to the past';
$il['news_pub_show_archive_future'] = 'From the current date into the future';
$il['news_pub_show_lenta_type_random'] = 'Random record';

$il['news_pub_show_sorting'] = 'Show sorting';
$il['news_pub_show_selection'] = 'Show selection';
$il['news_pub_show_selection_template'] = 'Template';

$il['news_menu_label'] = 'Management';
$il['news_menu_show_list'] = 'View';
$il['news_menu_between'] = 'During the period';
$il['news_menu_add_new'] = 'Add';
$il['news_menu_taxonomy'] = 'Taxonomy';
$il['news_menu_cats_list'] = 'Categories of records';
$il['news_menu_tags_list'] = 'List of tag';
$il['news_property_cats_list_label'] = 'Entries Category';
$il['news_property_tags_list_label'] = 'Entries Tags';
$il['news_delete_taxonomy_alert'] = 'Are you sure you want to remove the tag from the record';
$il['news_delete_taxonomy_success_msg'] = 'Tag was successfully deleted from the records';
$il['news_delete_taxonomy_error_msg'] = 'Tag removal error of recording';
$il['news_property_options_default'] = '- not selected -';

$il['news_menu_custom_fields'] = 'Custom fields';
$il['news_custom_fields_empty'] = '-not selected-';
$il['custom_fields_field_title'] = 'Field name';
$il['custom_fields_field_name'] = 'Identifier';
$il['custom_fields_field_type'] = 'Field Type';
$il['custom_fields_field_type_string'] = 'String';
$il['custom_fields_field_type_pagesite'] = 'Page';
$il['custom_fields_field_type_textarea'] = 'Text';
$il['custom_fields_field_type_select'] = 'Set of values (ENUM)';
$il['custom_fields_field_type_checkbox'] = 'Set of values (SET)';
$il['custom_fields_field_type_date'] = 'Date and time';
$il['custom_fields_field_type_fileselect'] = 'Select File';
$il['custom_fields_field_type_imageselect'] = 'Select Image';
$il['custom_fields_field_type_label'] = 'Every value from a new line';
$il['custom_fields_field_new'] = 'New Custom Field';
$il['custom_fields_field_edit'] = 'Editing an arbitrary field';
$il['custom_fields_field_order'] = 'Sorting order';
$il['custom_fields_order_delete'] = 'Delete Selected';
$il['news_custom_field_delete_confirm'] = 'Are you sure you want to delete an arbitrary field? All values will be lost. ';
$il['custom_fields_order_save'] = 'Save Order';
$il['custom_fields_order_add'] = 'Add Field';
$il['news_save'] = 'Save';
$il['news_property_seo_label'] = 'Meta tags (SEO)';

$il['news_tags_header_title'] = 'List of tag';
$il['news_add_tag_label'] = 'Add tag';
$il['news_add_new_tag_label'] = 'New Tag';
$il['news_edit_tag_label'] = 'Edit tag';
$il['news_tag_id'] = 'ID';
$il['news_tag_name'] = 'name';
$il['news_tag_description'] = 'Description';
$il['news_tag_use_count'] = 'Number of records';
$il['news_tags_table_empty'] = 'In the list of tags is empty';
$il['news_list_tag_action_edit'] = 'Edit';
$il['news_list_tag_action_del'] = 'Delete';
$il['news_tag_edit_save_label'] = 'Save';
$il['news_tag_del_alert'] = 'Are you sure you want to remove the tag from all entries !?';

$il['news_cats_header_title'] = 'List of headings';
$il['news_add_cat_label'] = 'Add category';
$il['news_add_new_cat_label'] = 'New heading';
$il['news_edit_cat_label'] = 'Edit Categories';
$il['news_cat_id'] = 'ID';
$il['news_cat_name'] = 'Name';
$il['news_cat_description'] = 'Description';
$il['news_cat_use_count'] = 'Number of records';
$il['news_cats_table_empty'] = 'In the list of empty columns';
$il['news_list_cat_action_edit'] = 'Edit';
$il['news_list_cat_action_del'] = 'Delete';
$il['news_cat_edit_save_label'] = 'Save';
$il['news_cat_del_alert'] = 'Are you sure you want to delete the column from all records !?';

// Public method to display the list of headings
$il['news_pub_show_cats'] = 'Display a list of headings';
$il['news_pub_show_cats_template'] = 'Template columns list';
$il['news_pub_show_cats_header'] = 'Title cats list';
$il['news_pub_show_cats_ifempty'] = 'Show empty columns';
$il['news_pub_show_tags'] = 'Display a list of tags';
$il['news_pub_show_tags_template'] = 'Template tag list';
$il['news_pub_show_tags_header'] = 'Title tag list';
$il['news_property_taxonomy']  = 'Use taxonomy for categories and tags';
$il['news_property_url_item_cats'] = 'Category identifier in the URL';
$il['news_property_url_item_tags'] = 'Tag identifier in the URL';
$il['news_pub_show_ifcount'] = 'Show a counter records';
$il['news_pub_show_infulltext']  = 'Show list for fulltext';

$il['news_show_list_action_lenta_on'] = 'Show in tape';
$il['news_show_list_action_lenta_off'] = 'Do not display in the Ribbon';
$il['news_show_list_action_available_on'] = 'Make visible';
$il['news_show_list_action_available_off'] = 'Make no visible';
$il['news_show_list_action_rss_on'] = 'Show in RSS';
$il['news_show_list_action_rss_off'] = 'Do not display in RSS';
$il['news_show_list_action_delete'] = 'Remove';
$il['news_show_list_action_move'] = 'Move';

// Заголовки в show_list
$il['news_item_id'] = 'ID';
$il['news_item_date'] = 'Date';
$il['news_item_datetime'] = 'Date and time';
$il['news_item_header'] = 'Title';
$il['news_item_available'] = 'Visibility';
$il['news_item_lenta'] = 'The film';
$il['news_item_rss'] = 'The RSS';
$il['news_item_author'] = 'Author';
$il['news_item_actions'] = 'Actions';

$il['news_property_date_label'] = 'Publication date';
$il['news_property_time_label'] = 'Publication time';
$il['news_property_available_label'] = 'News is active';
$il['news_property_lenta_label'] = 'Show in tape';
$il['news_property_delivery_label'] = 'Show the mailing list';
$il['news_property_rss_label'] = 'Show in RSS';
$il['news_property_header_label'] = 'Title';
$il['news_property_description_short_label'] = 'Short description';
$il['news_property_description_full_label'] = 'Full opisenie';
$il['news_property_author_label'] = 'Author';
$il['news_property_source_name_label'] = 'Source Name';
$il['news_property_source_url_label'] = 'The URL';
$il['news_property_image_label'] = 'Image';
$il['news_property_news_date_format'] = 'Date format (Y-m-d)';

$il['news_item_action_edit'] = 'Edit';
$il['news_item_action_remove'] = 'Remove';
$il['news_show_list_submit'] = 'OK';
$il['news_actions_with_selected'] = 'Actions with Selected:';
$il['news_submit_label'] = 'Retain';
$il['news_actions_simple'] = 'Actions';
$il['news_actions_advanced'] = 'Move to:';
$il['news_item_number'] = 'Number';
$il['news_menu_label1'] = 'The selection by date';
$il['news_select_between_label'] = 'Enter the desired date range';
$il['news_start_date'] = 'Enter the desired date range';
$il['news_end_date'] = 'Enter the desired date range';
$il['news_button_show'] = 'Display';

$il['news_property_pages_count'] = 'The number of pages in a block (N)';
$il['news_pub_pages_type'] = 'View pagination';
$il['news_pub_pages_get_block'] = 'Blocks of N pages';
$il['news_pub_pages_get_float'] = 'Tech. page is always at the center of a block of N pages';
$il['news_delete_confirm'] = 'Are you sure you want to delete the news?';

$il['news_pub_show_html_title'] = 'Show title';
$il['news_pub_show_meta_keywords'] = 'Meta-keywords';
$il['news_pub_show_meta_description'] = 'Meta-description';
$il['news_pub_pub_show_html_title_def'] = 'Title by default';
$il['news_pub_pub_show_meta_keywords_def'] = 'Meta-keywords by default';
$il['news_pub_pub_show_meta_description_def'] = 'Meta-description by default';
$il['news_property_html_title_label']       = 'SEO: Title';
$il['news_property_meta_keywords_label']    = 'SEO: Meta-keywords';
$il['news_property_meta_description_label'] = 'SEO: Meta-description';

$il['news_error_incorrect_datetime']= 'Incorrect date or time value';
