DROP TABLE IF EXISTS `%PREFIX%_catalog_catalog2_item_props_options`;
-- sqlseparator------------------------------------------------
CREATE TABLE `%PREFIX%_catalog_catalog2_item_props_options` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `item_id` int(11) DEFAULT NULL,
  `group_option_id` int(11) DEFAULT NULL,
  `option_choice` int(11) DEFAULT NULL,
  `prop_name` varchar(250) DEFAULT NULL,
  `option_value` text,
  `option_value_text` text,
  PRIMARY KEY (`id`),
  FULLTEXT KEY `option_value` (`option_value`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8
